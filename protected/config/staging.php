<?php
return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'), 
    array(
        'components'=>array(
            'db'=>array(
                // define DB connection used for development
				'connectionString' => 'mysql:host=localhost;dbname=ligaiaco_staging',
				'emulatePrepare' => true,
				'username' => 'ligaiaco_site',
				'password' => 'liGa!ac0',
				'charset' => 'utf8',
			    'enableParamLogging' => true,
            ),
        ),
    )
);