<?php
/* @var $this UserController */
/* @var $userModel User */
/* @var $form CActiveForm */
?>
<div class="layout-app">
    <!-- row-app -->
    <div class="row row-app">
        <!-- col -->
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom text-center bg-primary"><i class="fa fa-pencil"></i>Verification email sent!</h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="login">
                                <div class="placeholder text-center">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <div class="panel panel-default col-sm-8 col-sm-offset-2">
                                <div class="panel-body">
                                    <p>The verfication email has been sent to your email address. Please check your email.</p>
                                </div>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END row-app -->
</div>