<!-- row -->
<div class="row row-app margin-none">
	<!-- col -->
	<div class="col-md-12">
		<!-- col-separator -->
		<div class="col-separator col-separator-first border-none">
			<!-- col-table -->
			<div class="col-table">
				<!-- col-table-row -->
				<div class="col-table-row">
					<!-- col-app -->
					<div class="col-app">
						<!-- row-app -->
						<div class="row row-app margin-none">
							<div class="innerAll">
								<div class="relativeWrap">
									<div class="widget widget-tabs widget-tabs-double-2 widget-tabs-responsive widget-tabs-social-account">
										<!-- Tabs Heading -->
										<div class="widget-head">
											<ul>
												<li class="tab-stacked <?php if(isset($_POST['Investor'])) echo "active"?>"><a data-toggle="tab" href="#tabAccount" class="glyphicons user"><i></i><span>Account</span></a></li>
												<li class="tab-stacked tab-inverse"><a data-toggle="tab" href="#tabEmail" class="glyphicons envelope"><i></i><span>Email</span></a></li>
												<li class="tab-stacked <?php if(isset($_POST['User']['new_password'])) echo "active"?>"><a data-toggle="tab" href="#tabPassword" class="glyphicons lock"><i></i><span>Password</span></a></li>
												<li class="tab-stacked"><a data-toggle="tab" href="#tabPayments" class="glyphicons credit_card"><i></i><span>Payments</span></a></li>
											</ul>
										</div>
										<!-- // Tabs Heading END -->
										<div class="widget-body">
											<div class="tab-content">
												<!-- Tab content -->
												<div class="tab-pane <?php if(isset($_POST['Investor']) || empty($_POST)) echo "active" ?> animated fadeInUp" id="tabAccount">
													<div class="relativeWrap">
														<div class="widget widget-tabs widget-tabs-responsive">
															<!-- Tabs Heading -->
															<div class="widget-head">
																<ul>
																	<li class="active"><a data-toggle="tab" href="#tab-1" class="glyphicons edit"><i></i>Account Profile</a></li>
																	<li><a data-toggle="tab" href="#tab-2" class="glyphicons settings"><i></i>Owner Profile</a></li>
																</ul>
															</div>
															<!-- // Tabs Heading END -->
															<div class="widget-body">
																<div class="tab-content">
																	<!-- Tab content -->
																	<div id="tab-1" class="tab-pane active animated fadeInUp">
																		<?php $form=$this->beginWidget('CActiveForm', array(
																			'id'=>'profile-form',
																			// Please note: When you enable ajax validation, make sure the corresponding
																			// controller action is handling ajax validation correctly.
																			// There is a call to performAjaxValidation() commented in generated controller code.
																			// See class documentation of CActiveForm for details on this.
																			'enableAjaxValidation'=>false,
																			'htmlOptions'=>array(
																				'class'=>'form-horizontal innerAll'
																			)
																		)); ?>																	
																		<div class="col-md-12">
																			<div class="col-md-3">
																				<h5>Personal Details</h5>
																				<p>
																					Lorem ipsum dolor sit amet consectetu adi
																				</p>
																			</div>
																			<div class="col-md-4">
																				<div class="form-group">
																					<label class="control-label">First name</label>
																					<div class="input-group">
								                                                        <?php echo $form->textField($investorModel,'firstname', array('class'=>'form-control', 'value'=>$user->investor->firstname)); ?>
																						<span class="input-group-addon" title="First name"><i class="fa fa-question-circle"></i></span>
																					</div>
																					<?php echo $form->error($investorModel, 'firstname'); ?>
																				</div>
																				<div class="form-group">
																					<label class="control-label">Last name</label>
																					<div class="input-group">
								                                                        <?php echo $form->textField($investorModel,'lastname', array('class'=>'form-control', 'value'=>$user->investor->lastname)); ?>
																						<span class="input-group-addon" title="Last name"><i class="fa fa-question-circle"></i></span>
																					</div>
																					<?php echo $form->error($investorModel, 'lastname'); ?>
																				</div>
																				<div class="form-group">
																					<label class="control-label">Phone</label>
																					<div class="input-group">
																						<span class="input-group-addon"><i class="fa fa-phone"></i></span>
								                                                        <?php echo $form->textField($investorModel,'phone', array('class'=>'form-control', 'value'=>$user->investor->phone)); ?>
																						<span class="input-group-addon" title="Mobile or Landline Number"><i class="fa fa-question-circle"></i></span>
																						<?php echo $form->error($investorModel, 'phone'); ?>
																					</div>
																				</div>
																			</div>
																			<div class="clearfix">
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-3">
																				<h5>Address</h5>
																				<p>
																					Lorem ipsum dolor sit amet consectetu adi
																				</p>
																			</div>
																			<div class="col-md-4">
																				<div class="form-group">
																					<label class="control-label">Country</label>
																					<div class="row">
																						<?php echo $form->dropDownList(
																							$user->investor,  
																							'country',
																							CHtml::listData(Geolocation::getCountries(),
																							'code',
																							'name'),
																							array(
																								'empty'=>'Select Country',
																								'class'=>'selectpicker')); ?> 
																					</div>
																					<?php echo $form->error($investorModel, 'country'); ?>
																				</div>
																				<div class="form-group">
																					<label class="control-label">Region</label>
																					<div class="input-group">
								                                                        <?php echo $form->textField($investorModel,'region', array('class'=>'form-control', 'value'=>$user->investor->region)); ?>
																						<span class="input-group-addon" title="Region"><i class="fa fa-question-circle"></i></span>
																					</div>
																					<?php echo $form->error($investorModel, 'region'); ?>
																				</div>
																				<div class="form-group">
																					<label class="control-label">City</label>
								                                                    <?php echo $form->textField($investorModel,'city', array('class'=>'form-control', 'value'=>$user->investor->city)); ?>
								                                                    <?php echo $form->error($investorModel, 'city'); ?>
																				</div>
																				
																				<div class="form-group">
																					<label class="control-label">Zip code</label>
																					<div class="input-group">
																						<?php echo $form->textField($investorModel,'zip', array('class'=>'form-control', 'value'=>$user->investor->zip)); ?>
																						<span class="input-group-addon" title="Zip Code"><i class="fa fa-question-circle"></i></span>
																					</div>
																					<?php echo $form->error($investorModel, 'zip'); ?>
																				</div>
																				<div class="form-group">
																					<label class="control-label">Date of Birth</label>
																					<div class="input-group">
																						<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
																						    'model' => $investorModel,
																						    'attribute' => 'dob',
																							'options' => array(
																								'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
																								'changeMonth'=>true,
																								'changeYear'=>true,
																								'yearRange'=>'1920:2014',
																								'maxDate'=>'-10Y',
																							),
																						    'htmlOptions' => array(
																						        'size' => '10',         // textField size
																						        'maxlength' => '10',    // textField maxlength
																								'class' => 'form-control',
																								'value' => $user->investor->dob,
																						    ),
																						));	?>
																						<span class="input-group-addon" title="Date of Birth"><i class="fa fa-question-circle"></i></span>
																					</div>
																					<?php echo $form->error($investorModel, 'dob'); ?>
																				</div>
																				<div class="text-left">
																					<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
																					<button class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
																				</div>
																			</div>
																		</div>
																			<div class="clearfix">
																			</div>
																			<?php $this->endWidget(); ?>
																		</div>
																		<!-- // Tab content END -->
																		<!-- Tab content -->
																		<div id="tab-2" class="tab-pane animated fadeInUp">
																			<div class="row">
																				<div class="innerAll">
																					<div class="media">
																						<button class="pull-right btn btn-default btn-sm"><i class="fa fa-fw fa-edit"></i> Edit</button>
																						<img width="100" alt="" class="thumb pull-left animated fadeInDown" src="../assets/images/people/250/2.jpg" style="visibility: visible;">
																						<div class="media-body innerAll half">
																							<h4 class="media-heading text-large"><?php echo $user->investor->firstname?>&nbsp;<?php echo $user->investor->lastname?></h4>
																							<p>
																								Living in <?php echo $user->investor->city?>, <?php print_r(Geolocation::getCountryName($user->investor->country)) ?><br>
																								Investor 1234567890
																							</p>
																						</div>
																					</div>
																				</div>
																				<div class="col-separator-h box">
																				</div>
																				<div class="row row-merge margin-none">
																					<div class="col-md-4">
																						<div class="innerAll text-center">
																							<span class="text-large strong">4</span><br>
																							 Trees
																						</div>
																					</div>
																					<div class="col-md-4">
																						<div class="innerAll text-center">
																							<span class="text-large strong">$24,567</span><br>
																							 Estimate value today
																						</div>
																					</div>
																					<div class="col-md-4">
																						<div class="innerAll text-center">
																							<span class="text-large strong">$256</span><br>
																							 Investment
																						</div>
																					</div>
																				</div>
																				<div class="col-separator-h box">
																				</div>
																				<h4 class="innerAll margin-none border-bottom"><i class="fa fa-fw fa-clock-o pull-right text-muted"></i> History</h4>
																				<ul class="timeline-activity list-unstyled">
																					<li style="visibility: visible;" class="animated bounceInUp">
																					<i class="list-icon fa fa-repeat"></i>
																					<div class="block">
																						<div class="caret">
																						</div>
																						<div class="box-generic bg-primary-light">
																							<div class="timeline-top-info border-bottom">
																								<a class="text-regular" href="">Upcoming investment</a><span class="text-muted2">(Recurring)</span> 2 Trees <i class="fa fa-stethoscope"></i> 1 year old.
																							</div>
																							<div class="innerAll half inline-block">
																								<i class="fa fa-calendar text-primary"></i> 3 October 2013
																							</div>
																						</div>
																						<div class="separator bottom">
																						</div>
																					</div>
																					</li>
																					<li style="visibility: visible;" class="animated bounceInUp">
																					<i class="list-icon fa fa-money"></i>
																					<div class="block">
																						<div class="caret">
																						</div>
																						<div class="box-generic">
																							<div class="timeline-top-info border-bottom">
																								<a class="text-regular" href="">Investment</a> (Recurring) 2 Trees <i class="fa fa-repeat"></i> 1 year old
																							</div>
																							<div class="innerAll half inline-block">
																								<i class="fa fa-calendar"></i> 2 October 2013
																							</div>
																						</div>
																						<div class="separator bottom">
																						</div>
																					</div>
																					<div class="block block-inline">
																						<div class="box-generic">
																							<div class="innerT innerR">
																								<button class="btn btn-primary btn-xs pull-right"><i class="fa fa-eye"></i></button>
																								<div class="media inline-block margin-none">
																									<div class="innerLR">
																										<i class="fa fa-file-text-o pull-left text-primary fa-2x"></i>
																										<div class="media-body pull-left">
																											<div>
																												<a class="strong text-regular" href="">Reciept</a>
																											</div>
																											<span>24 KB</span>
																										</div>
																										<div class="clearfix">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="block block-inline">
																						<div class="box-generic">
																							<div class="innerT innerR">
																								<button class="btn btn-primary btn-xs pull-right"><i class="fa fa-eye"></i></button>
																								<div class="media inline-block margin-none">
																									<div class="innerLR">
																										<i class="fa fa-file-text-o pull-left text-primary fa-2x"></i>
																										<div class="media-body pull-left">
																											<div>
																												<a class="strong text-regular" href="">Documentation</a>
																											</div>
																											<span>24 KB</span>
																										</div>
																										<div class="clearfix">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					</li>
																					<li style="visibility: visible;" class="animated bounceInUp">
																					<i class="list-icon fa fa-money"></i>
																					<div class="block">
																						<div class="caret">
																						</div>
																						<div class="box-generic">
																							<div class="timeline-top-info border-bottom">
																								<a class="text-regular" href="">Investment</a> (Recurring) 2 Trees <i class="fa fa-repeat"></i> 1 year old
																							</div>
																							<div class="innerAll half inline-block">
																								<i class="fa fa-calendar"></i> 2 October 2013
																							</div>
																						</div>
																						<div class="separator bottom">
																						</div>
																					</div>
																					<div class="block block-inline">
																						<div class="box-generic">
																							<div class="innerT innerR">
																								<button class="btn btn-primary btn-xs pull-right"><i class="fa fa-eye"></i></button>
																								<div class="media inline-block margin-none">
																									<div class="innerLR">
																										<i class="fa fa-file-text-o pull-left text-primary fa-2x"></i>
																										<div class="media-body pull-left">
																											<div>
																												<a class="strong text-regular" href="">Reciept</a>
																											</div>
																											<span>24 KB</span>
																										</div>
																										<div class="clearfix">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="block block-inline">
																						<div class="box-generic">
																							<div class="innerT innerR">
																								<button class="btn btn-primary btn-xs pull-right"><i class="fa fa-eye"></i></button>
																								<div class="media inline-block margin-none">
																									<div class="innerLR">
																										<i class="fa fa-file-text-o pull-left text-primary fa-2x"></i>
																										<div class="media-body pull-left">
																											<div>
																												<a class="strong text-regular" href="">Documentation</a>
																											</div>
																											<span>24 KB</span>
																										</div>
																										<div class="clearfix">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																					</li>
																				</ul>
																			</div>
																		</div>
																		<!-- // Tab content END -->
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- // Tab content END -->
													<!-- Tab content -->
													<div class="tab-pane widget-body-regular animated fadeInUp" id="tabEmail">
														<div class="relativeWrap innerAll">
															<form class="form-horizontal">
																<div class="row">
																	<div class="col-md-3">
																		<h5>Email preferences</h5>
																		<p>
																			Lorem ipsum dolor sit amet consectetu adi
																		</p>
																	</div>
																	<div class="col-md-4">
																		<label class="control-label">Email</label>
																		<div class="input-group">
																			<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
																			<input class="form-control" placeholder="contact@mosaicpro.biz"/>
																			<span class="input-group-addon" title="Email"><i class="fa fa-question-circle"></i></span>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-3">
																		<h5>Updates</h5>
																		<p>
																			Lorem ipsum dolor sit amet consectetu adi
																		</p>
																	</div>
																	<div class="col-md-4">
																		<div class="checkbox">
																			<label class="checkbox-custom">
																			<input type="checkbox" checked="checked" name="updates">
																			<i class="fa fa-fw fa-square-o checked"></i><b>Sit amet condicti</b><br>
																			 Lorem ipsum dolor sit amet </label>
																		</div>
																		<div class="input-group">
																			<div class="checkbox">
																				<label class="checkbox-custom">
																				<input type="checkbox" name="updates">
																				<i class="fa fa-fw fa-square-o"></i><b>Sit amet condicti</b><br>
																				 Lorem ipsum dolor sit amet </label>
																			</div>
																		</div>
																		<div class="checkbox">
																			<label class="checkbox-custom">
																			<input type="checkbox" checked="checked" name="updates">
																			<i class="fa fa-fw fa-square-o checked"></i><b>Sit amet condicti</b><br>
																			 Lorem ipsum dolor sit amet </label>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-3">
																		<h5>Notifications</h5>
																		<p>
																			Lorem ipsum dolor sit amet consectetu adi
																		</p>
																	</div>
																	<div class="col-md-4">
																		<div class="checkbox">
																			<label class="checkbox-custom">
																			<input type="checkbox" checked="checked" name="notifications">
																			<i class="fa fa-fw fa-square-o checked"></i><b>Sit amet condicti</b><br>
																			 Lorem ipsum dolor sit amet </label>
																		</div>
																		<div class="checkbox">
																			<label class="checkbox-custom">
																			<input type="checkbox" name="notifications">
																			<i class="fa fa-fw fa-square-o"></i><b>Sit amet condicti</b><br>
																			 Lorem ipsum dolor sit amet </label>
																		</div>
																		<div class="checkbox">
																			<label class="checkbox-custom">
																			<input type="checkbox" checked="checked" name="notifications">
																			<i class="fa fa-fw fa-square-o checked"></i><b>Sit amet condicti</b><br>
																			 Lorem ipsum dolor sit amet </label>
																		</div>
																	</div>
																</div>
																<div class="row">
																	<div class="text-left">
																		<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
																		<button class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
																	</div>
																</div>
															</form>
														</div>
													</div>
													<!-- // Tab content END -->
													<!-- Tab content -->
													<div class="<?php if(isset($_POST['User']['new_password'])) echo "active"?> tab-pane widget-body-regular animated fadeInUp" id="tabPassword">
														<div class="col-md-12">
															<h4 class="innerTB">Change password</h4>
																<?php $form=$this->beginWidget('CActiveForm', array(
																	'id'=>'profile-form',
																	// Please note: When you enable ajax validation, make sure the corresponding
																	// controller action is handling ajax validation correctly.
																	// There is a call to performAjaxValidation() commented in generated controller code.
																	// See class documentation of CActiveForm for details on this.
																	'enableAjaxValidation'=>false,
																	'htmlOptions'=>array(
																		'class'=>'form-horizontal innerAll'
																	)
																)); ?>	
																<?php echo $form->errorSummary($user)?>
																<div class="form">
																	<div class="form-group">
																		<label class="col-sm-2 control-label">Current password</label>
																		<div class="col-sm-4">
																			<?php echo $form->passwordField($user,'current_password', array('class'=>'form-control')); ?>
																			<?php echo $form->error($user, 'current_password'); ?>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-2 control-label">New password</label>
																		<div class="col-sm-4">
																			<?php echo $form->passwordField($user,'new_password', array('class'=>'form-control')); ?>
																			<?php echo $form->error($user, 'new_password'); ?>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-2 control-label">Retype password</label>
																		<div class="col-sm-4">
																			<?php echo $form->passwordField($user,'password_repeat', array('class'=>'form-control')); ?>
																			<?php echo $form->error($user, 'password_repeat'); ?>
																		</div>
																	</div>
																	<div class="col-sm-offset-2 text-left">
																		<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
																		<button class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
																	</div>
																	<div class="clearfix">
																	</div>
																</div>
															<?php $this->endWidget()?>
														</div>
														<div class="clearfix">
														</div>
													</div>
													<!-- // Tab content END -->
													<!-- Tab content -->
													<div class="tab-pane widget-body-regular animated fadeInUp" id="tabPayments">
														<h4 class="innerTB">Payment &amp; Billing Information</h4>
														<div class="row">
															<div class="col-md-9">
																<div class="box-generic bg-primary padding-none">
																	<div class="innerAll half pull-right">
																		<a href="#" class="btn btn-default btn-sm"><i class="fa fa-times "></i></a>
																	</div>
																	<h4 class="text-white strong margin-none innerAll pull-left"><i class="fa fa-credit-card innerR"></i> XXXX XXXX XXXX XXXX 1209</h4>
																	<div class="clearfix">
																	</div>
																</div>
																<div class="box-generic padding-none">
																	<div class="pull-right btn-group btn-group-sm innerAll half">
																		<a href="#" class="btn btn-default"><i class="fa fa-check "></i> Make Primary</a>
																		<a href="" class="btn btn-default"><i class="fa fa-times"></i></a>
																	</div>
																	<h4 class="strong margin-none innerAll pull-left"><i class="fa fa-credit-card innerR"></i> XXXX XXXX XXXX XXXX 1209</h4>
																	<div class="clearfix">
																	</div>
																</div>
																<div class="box-generic padding-none">
																	<h5 class="strong innerAll border-bottom margin-none bg-gray">Add New Card</h5>
																	<div class="innerAll inner-2x">
																		<form action="../shop/checkout_confirmation.html?lang=en&amp;skin=bud-green&amp;v=v1.9.7-rc1">
																			<div class="form-group">
																				<label for="exampleInputEmail1">Card Number</label>
																				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Card Number">
																			</div>
																			<div class="form-group">
																				<div class="row">
																					<div class="col-xs-6">
																						<label for="exampleInputPassword1">Expiry Date</label>
																						<div class="row margin-none">
																							<div class="col-xs-6 padding-none">
																								<select class="form-control selectpicker">
																									<option>1</option>
																									<option>2</option>
																									<option>3</option>
																									<option>4</option>
																									<option>5</option>
																									<option>6</option>
																									<option>7</option>
																									<option>8</option>
																									<option>9</option>
																									<option>10</option>
																									<option>11</option>
																								</select>
																							</div>
																							<div class="col-xs-6 padding-none">
																								<select class="form-control selectpicker">
																									<option>2013</option>
																									<option>2014</option>
																									<option>2015</option>
																									<option>2016</option>
																									<option>2017</option>
																									<option>2018</option>
																								</select>
																							</div>
																						</div>
																					</div>
																					<div class="col-xs-6">
																						<label for="exampleInputPassword1">Security Code <i class="fa fa-question-circle"></i></label>
																						<input type="password" class="form-control" placeholder="CVV / CVV2">
																					</div>
																				</div>
																			</div>
																			<div class="form-group">
																				<label for="exampleInputEmail1">Cardholder Name</label>
																				<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name on Card">
																			</div>
																			<div class="text-right">
																				<button type="submit" class="btn btn-primary">Make a Payment </button>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
															<div class="col-md-3">
																<div class="box-generic strong">
																	<i class="fa fa-user fa-3x pull-left text-faded"></i> Adrian Demian <br/><small class="text-faded">12 Nov 2013</small>
																</div>
																<div class="box-generic padding-none">
																	<div class="innerAll border-bottom">
																		<a href="" class="pull-right text-primary"><i class="fa fa-pencil"></i></a>
																		<h4 class="panel-title strong">Billing Address </h4>
																	</div>
																	<div class="innerAll">
																		<i class="fa fa-building pull-left fa-3x"></i><span>129 Longford Terrace, Dublin, Ireland</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- // Tab content END -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- // END row -->
						</div>
						<!-- // END col-app -->
					</div>
					<!-- // END col-table-row -->
				</div>
				<!-- // END col-table -->
			</div>
			<!-- // END col-separator -->
		</div>
		<!-- // END col -->
	</div>
	<!-- // END row-app -->
