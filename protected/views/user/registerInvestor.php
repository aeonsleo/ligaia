<?php
/* @var $this UserController */
$this->pageTitle=Yii::app()->name . ' - Register';
?>
<div class="layout-app">
	<!-- row-app -->
	<div class="row row-app">
		<!-- col -->
		<!-- col-separator.box -->
		<div class="col-separator col-unscrollable box">
			<!-- col-table -->
			<div class="col-table">
				<!-- col-table-row -->
				<div class="col-table-row">
					<!-- col-app -->
					<div class="col-app col-unscrollable">
						<!-- col-app -->
						<div class="col-app">
							<div class="login">
								<div class="placeholder text-center">
									<i class="fa fa-pencil"></i>
								</div>
								<div class="text-center">
								<div class="col-sm-8">
									<div class="panel-body">
										<?php echo $this->renderPartial('_registerInvestor', array('userModel' => $userModel, 'investorModel' => $investorModel)); ?>
									</div>
								</div>
								</div>
								<div class="clearfix">
								</div>
							</div>
						</div>
						<!-- // END col-app -->
					</div>
					<!-- // END col-app.col-unscrollable -->
				</div>
				<!-- // END col-table-row -->
			</div>
			<!-- // END col-table -->
		</div>
		<!-- // END col-separator.box -->
	</div>
</div>        