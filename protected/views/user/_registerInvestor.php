                                            <?php $form=$this->beginWidget('CActiveForm', array(
                                                'id'=>'register-form',
                                                'enableClientValidation'=>true,
                                                'clientOptions'=>array(
                                                    'validateOnSubmit'=>true,
                                                ),
                                                'htmlOptions'=>array(
                                                    'class'=>'form-horizontal',
                                                ),
                                            )); ?>
                                                <p class="note">Fields with <span class="required">*</span> are required.</p>

                                                <div class="form-group">
                                                    <?php echo $form->labelEx($userModel,'username', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($userModel,'username', array('class'=>'form-control')); ?>
                                                        <?php echo $form->error($userModel,'username'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($userModel,'password', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->passwordField($userModel,'password', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($userModel,'password'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($userModel,'password_repeat', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->passwordField($userModel,'password_repeat', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($userModel,'password_repeat'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($userModel,'email', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($userModel,'email', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($userModel,'email'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'firstname', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'firstname', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'firstname'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'lastname', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'lastname', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'lastname'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'company', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'company', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'company'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'address_line_1', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'address_line_1', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'address_line_1'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'address_line_2', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'address_line_2', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'address_line_2'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'address_line_3', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'address_line_3', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'address_line_3'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'city', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'city', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'city'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel,'zip', array('class'=>'col-sm-3 control-label')); ?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->textField($investorModel,'zip', array('class'=>'form-control')); ?>
                                                    </div>
                                                    <?php echo $form->error($investorModel,'zip'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo $form->labelEx($investorModel, 'country', array('class'=>'col-sm-3 control-label'))?>
                                                    <div class="col-sm-8">
                                                        <?php echo $form->dropDownList(
                                                                $investorModel,
                                                                'country', 
                                                                CHtml::listData(Geolocation::getCountries(),
                                                                'code', 
                                                                'name'),
                                                                array('empty'=>'Select Country', 'class'=>'selectpicker', 'data-style'=>'btn-default')); ?>   
                                                    </div>
                                                </div>
                                            <?php echo CHtml::submitButton('Register', array('class'=>'btn btn-primary btn-block')); ?>

                                            <?php $this->endWidget(); ?>
