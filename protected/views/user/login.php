<div class="layout-app">
	<!-- row-app -->
	<div class="row row-app">
		<!-- col -->
		<!-- col-separator.box -->
		<div class="col-separator col-unscrollable box">
			<!-- col-table -->
			<div class="col-table">
				<!-- col-table-row -->
				<div class="col-table-row">
					<!-- col-app -->
					<div class="col-app col-unscrollable">
						<!-- col-app -->
						<div class="col-app">
							<div class="login">
								<div class="placeholder text-center">
									<i class="fa fa-lock"></i>
								</div>
								<div class="col-sm-5">
									<div class="panel-body">
										<button type="submit" class="btn btn-social btn-twitter btn-block text-left"><i class="fa fa-twitter"></i>Sign in with Twitter</button>
										<button type="submit" class="btn btn-social btn-facebook btn-block text-left"><i class="fa fa-facebook"></i>Sign in with Facebook</button>
										<button type="submit" class="btn btn-social btn-google btn-block text-left"><i class="fa fa-google-plus"></i>Sign in with Google</button>
										<a href="#">OpenID?</a>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="panel-body">
										<div class="v-lines">
											<div class="v-line">
											</div>
											<div class="v-text">
												OR
											</div>
											<div class="v-line">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="panel-body">
										<?php $form=$this->beginWidget('CActiveForm', array(
											'id'=>'login-form',
                                             'enableClientValidation'=>true,
                                             'clientOptions'=>array(
                                            	  'validateOnSubmit'=>true,
                                             ),
                                        )); ?>
											<div class="form-group">
												<?php echo $form->textField($model,'username', array('class'=>'form-control', 'placeholder'=>'Enter email')); ?>
												<?php echo $form->error($model,'username'); ?>
											</div>
											<div class="form-group">
												<?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'placeholder'=>'password')); ?>
												<?php echo $form->error($model,'password'); ?>
											</div>
											<button type="submit" class="btn btn-original-primary btn-block">Login</button>
											<div class="col-sm-12">
												<div class="checkbox">
													<label>
													<input type="checkbox"> Remember my details </label>
												</div>
											</div>
											<div class="col-sm-12 text-right">
												<a href="#">Forgot Password?</a>
											</div>
										<?php $this->endWidget(); ?>
									</div>
								</div>
								<div class="col-sm-4 col-sm-offset-4 text-center">
									<div class="innerAll">
										<a href="<?php print Yii::app()->baseUrl?>/user/register" class="btn btn-info">Create a new account? <i class="fa fa-pencil"></i></a>
										<div class="separator">
										</div>
									</div>
								</div>
								<div class="clearfix">
								</div>
							</div>
						</div>
						<!-- // END col-app -->
					</div>
					<!-- // END col-app.col-unscrollable -->
				</div>
				<!-- // END col-table-row -->
			</div>
			<!-- // END col-table -->
		</div>
		<!-- // END col-separator.box -->
	</div>
</div>