<?php
/* @var $this UserController */
/* @var $userModel User */
/* @var $form CActiveForm */
?>
<div class="layout-app">
    <!-- row-app -->
    <div class="row row-app">
        <!-- col -->
        <!-- col-separator.box -->
        <div class="col-separator col-unscrollable box">
            <!-- col-table -->
            <div class="col-table">
                <h4 class="innerAll margin-none border-bottom text-center bg-primary"><i class="fa fa-pencil"></i>Send Verification Email</h4>
                <!-- col-table-row -->
                <div class="col-table-row">
                    <!-- col-app -->
                    <div class="col-app col-unscrollable">
                        <!-- col-app -->
                        <div class="col-app">
                            <div class="login">
                                <div class="placeholder text-center">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <div class="panel panel-default col-sm-8 col-sm-offset-2">
                                <div class="panel-body">
                                	<p>The account could not be verified</p>
                                    <p><?php echo CHtml::link('Resend', array('investor/sendVerificationEmail')); ?> the verification link in email</p>
                                	<?php 
                                		$form = $this->beginWidget('CActiveForm', array(
                                			'id'=>'send-verification-email',
                                			'enableClientValidation'=>true,
                                			'clientOptions'=>array(
                                				'validateOnSubmit'=>true
                                			),
                                			'htmlOptions'=>array(
                                				'class'=>'form-horizontal'
                                			)
                                		));
                                	?>
                                	<div class="form-group">
	                                	<?php echo $form->labelEx($model,'email', array('class'=>'col-sm-3 control-label')); ?>
	                                	<div class="col-sm-8">
		                                	<?php echo $form->error($model,'email'); ?>
	                                		<?php echo $form->textField($model,'email', array('class'=>'form-control')); ?>
	                                	</div>
                                	</div>                                	
                                	
                                	<?php echo CHtml::submitButton('Send', array('class'=>'btn btn-primary btn-block')); ?>
                                	<?php $this->endWidget();?>
                                </div>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                        <!-- // END col-app -->
                    </div>
                    <!-- // END col-app.col-unscrollable -->
                </div>
                <!-- // END col-table-row -->
            </div>
            <!-- // END col-table -->
        </div>
        <!-- // END col-separator.box -->
    </div>
    <!-- // END row-app -->
</div>