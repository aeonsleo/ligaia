<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if !IE]><!-->
<html class="navbar-sticky footer-sticky">
<!-- <![endif]-->
<head>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/less/admin/module.admin.stylesheet-complete.sidebar_type.fusion.less" />
	-->
<!--[if lt IE 9]><link rel="stylesheet" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
<link rel="stylesheet" href="<?php echo yii::app()->
request->baseUrl ?>/coral/assets/css/admin/module.admin.stylesheet-complete.layout_fixed.true.css" /> 
<link rel="stylesheet" href="<?php echo yii::app()->request->baseUrl?>/css/styles.css" /> 
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<?php $baseUrl = yii::app()->request->baseUrl ?>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery/jquery.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery/jquery-migrate.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/modernizr/modernizr.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/less-js/less.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery-ui/js/jquery-ui.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v1.9.6&sv=v0.0.1"></script>
<!-- <script src="<?php echo yii::app()->request->baseUrl ?>/js/admin.js""></script>-->
<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body class=" menu-right-hidden">
<!-- Main Container Fluid -->
<div class="menu-hidden">
	<!-- Content -->
	<div id="content">
		<?php $this->widget('HeaderWidget')?>
		<!-- // END navbar -->
		<div id="menu-top" class="menu-top"></div>		
		<div class="layout-app">
		<div class="container">
			<div class="row row-app margin-none">
				<div class="col-md-12">
					<div class="col-separator col-separator-first border-none">
					<?php if(Yii::app()->user->hasFlash('success')):?>
						<div class="alert alert-success">
							<button data-dismiss="alert" class="close" type="button">x</button>
							<strong>Success!</strong><?php echo Yii::app()->user->getFlash('success'); ?>
						</div>
					<?php elseif(Yii::app()->user->hasFlash('error')):?>
						<div class="alert alert-danger">
							<button data-dismiss="alert" class="close" type="button">x</button>
							<strong>Error!</strong><?php echo Yii::app()->user->getFlash('error'); ?>
						</div>
					<?php endif; ?>				
					</div>
				</div>
			</div>
			<?php echo $content ?>
		</div>
		</div>
		<div id="footer" class="footer">
			<p>Ligaia<sup>&copy;</sup>: All rights reserved. 2014.</p>
		</div>
	</div>
	<!-- // Content END -->
	<div class="clearfix">
	</div>
	<!-- // Sidebar menu & content wrapper END -->
	<!-- Footer -->
	<!-- // Footer END -->
</div>
<!-- // Main Container Fluid END -->

<!-- Global -->
<script data-id="App.Config">
	var basePath = '',
		commonPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/',
		rootPath = '<?php echo yii::app()->request->baseUrl?>',
		DEV = false,
		componentsPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/',
		layoutApp = false,
		module = 'admin';
	var primaryColor = '#eb6a5a',
		dangerColor = '#b55151',
		successColor = '#609450',
		infoColor = '#4a8bc2',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	var themerPrimaryColor = primaryColor;
	</script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/breakpoints/breakpoints.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/custom/js/datatables.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/classic/assets/js/tables-classic.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/sidebar.main.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/sidebar.collapse.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/core.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/animations.init.js?v=v1.9.6"></script>
</body>
</html>