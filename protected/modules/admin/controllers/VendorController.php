<?php

class VendorController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = Vendor::model()->with('productLots')->find('t.user_id=:user_id', array(':user_id'=>$id));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionNew()
	{
		$model=new Vendor;
		$userModel = new User;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['User']) && isset($_POST['Vendor']))
		{
			$userModel->attributes = $_POST['User'];
			$userModel->username = $this->genRandomString(10);
			$userModel->password = $userModel->password_repeat = 'ligaia';
			
			/* Get the status */
			$criteria = new CDbCriteria;
			$criteria->condition = "status_type = 'user' AND status = 'inactive'";
			$status = Status::model()->find($criteria);
			$userModel->status_id = $status->id;
			
			$transaction = Yii::app()->db->beginTransaction();
			
			//Formatter::arrayFormat($userModel); exit;
			if($userModel->save())
			{
				$model->attributes=$_POST['Vendor'];
				$model->user_id = $userModel->getPrimaryKey();

				if($model->save())
				{
					$transaction->commit();
					$this->redirect(array('view','id'=>$model->user_id));
				}
				else
				{
					//Formatter::arrayFormat($model->getErrors());
					$transaction->rollback();
				}
			}
			
			//Formatter::arrayFormat($userModel->getErrors());
		}

		$this->render('create',array(
			'model'=>$model,
			'userModel'=>$userModel
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Vendor']))
		{
			$model->attributes=$_POST['Vendor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Vendor',
				array(
					'criteria'=>array(
						'with'=>array(
							'partner'
						)
					)
				)
			);
		$this->render('index',array(
			'vendors'=>$dataProvider->GetData(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Vendor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vendor']))
			$model->attributes=$_GET['Vendor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	protected function genRandomString($length=10, $chars='', $type=array()) {
		//initialize the characters
		$alphaSmall = 'abcdefghijklmnopqrstuvwxyz';
		$alphaBig = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '0123456789';
		$othr = '`~!@#$%^&*()/*-+_=[{}]|;:",<>.\/?' . "'";
	
		$characters = "";
		$string = '';
		//defaults the array values if not set
		isset($type['alphaSmall']) ? $type['alphaSmall']: $type['alphaSmall'] = true; //alphaSmall - default true
		isset($type['alphaBig']) ? $type['alphaBig']: $type['alphaBig'] = true; //alphaBig - default true
		isset($type['num']) ? $type['num']: $type['num'] = true; //num - default true
		isset($type['othr']) ? $type['othr']: $type['othr'] = false; //othr - default false
		isset($type['duplicate']) ? $type['duplicate']: $type['duplicate'] = true; //duplicate - default true
	
		if (strlen(trim($chars)) == 0) {
			$type['alphaSmall'] ? $characters .= $alphaSmall : $characters = $characters;
			$type['alphaBig'] ? $characters .= $alphaBig : $characters = $characters;
			$type['num'] ? $characters .= $num : $characters = $characters;
			$type['othr'] ? $characters .= $othr : $characters = $characters;
		}
		else
			$characters = str_replace(' ', '', $chars);
	
		if($type['duplicate'])
		for (; $length > 0 && strlen($characters) > 0; $length--) {
			$ctr = mt_rand(0, (strlen($characters)) - 1);
			$string .= $characters[$ctr];
		}
		else
			$string = substr (str_shuffle($characters), 0, $length);
	
		return $string;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Vendor the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Vendor::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Vendor $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='vendor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
