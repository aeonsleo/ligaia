<?php

/**
 * Dashboard Controller
 * 
 * @author Abhishek Srivastava <abhishek.sr017@gmail.com>
 *
 */
class DashboardController extends AdminController
{
	public $layout = 'columnx';
	
	public function actionIndex()
	{
		$userId = Yii::app()->user->getId();
		$user = User::model()->findByPk($userId);
		$this->render('index', array('user'=>$user));
	}
}