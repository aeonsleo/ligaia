<?php

class VariantController extends AdminController
{
	public $layout='/layouts/admin_tools';
	
	public function actionNew()
	{
		$model = new ProductVariation;
		
		if(isset($_POST['ProductVariation']))
		{
			$model->attributes = $_POST['ProductVariation'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',' Product Variant have been saved');
				$this->redirect(array('index'));
			}
			Formatter::pretty($model->getErrors());
				
		}
		
		$this->render('create', array('model'=>$model));		
	}
	
	public function actionIndex()
	{
		$variantModel = new ProductVariation;
		$variants = $variantModel->findAll();
		
	
		$this->render('index', array('variants'=>$variants));		
	}
	
	public function actionEdit()
	{
		$id = $_GET['id'];
		$model = ProductVariation::model()->findByPk($id);
		
		if(isset($_POST['ProductVariation']))
		{
			$model->attributes = $_POST['ProductVariation'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',' Product Variant have been saved');
				$this->redirect(array('index'));
			}
			Formatter::pretty($model->getErrors());
		
		}	

		$this->render('edit', array('model'=>$model));
	}
	
}