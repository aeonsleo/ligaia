<?php

class ToolsController extends AdminController
{
	public $layout='/layouts/admin_tools';
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionNewCategory()
	{
		$model=new ProductCategory;
		if(isset($_POST['ProductCategory']))
		{
			$model->attributes = $_POST['ProductCategory'];
			$model->parent_id = 0; // Defalt value for single level categories. To be amended if multiple level categories are used.
			
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			
			if($model->save())
			{
				$this->redirect(array('category/index'));
			}
			
			//Formatter::pretty($model->getErrors());
		}
		
		$this->render('create_category',array(
			'model'=>$model,
		));
	}

	/**
	 * Add a new product variant
	 */
	public function actionNewVariant()
	{
		$model = new ProductVariation;
	
		if(isset($_POST['ProductVariation']))
		{
			$model->attributes = $_POST['ProductVariation'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',' Product Variant have been saved');
				$this->redirect(array('index'));
			}
			Formatter::pretty($model->getErrors());
					
		}
	
		$this->render('create-variant', array('model'=>$model));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$variantModel = new ProductVariation;
		$criteria = new CDbCriteria;
		$criteria->limit = 5;
		
		$variants = $variantModel->findAll($criteria);
		
		$categoryModel = new ProductCategory;
		$categories = $categoryModel->findAll($criteria);
		
		$typeModel = new ProductType;
		$types = $typeModel->findAll($criteria);
		
		$this->render('index', array('variants'=>$variants, 'categories'=>$categories, 'types'=>$types));		
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Partner('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Partner']))
			$model->attributes=$_GET['Partner'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Partner the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Partner::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Partner $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='partner-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
