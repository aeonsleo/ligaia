<?php

class AuthController extends CController
{
	public $layout = 'columnx';
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('login'),
				'users'=>array('*'),
			)
		);	
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if(Yii::app()->user->checkAccess('superadmin'))
		{
			$this->redirect(array('/admin/dashboard/index'));
		}
		
		$model=new LoginForm;
	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				$this->redirect(array('/admin/dashboard/index'));
			}
		}
		
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(false);
        $this->redirect(array('/admin/auth/login'));	
	}
	
    public function actionRegister()
    {
    
    }
    
    public function actionUnauthorized()
    {
    	$this->render('not-authorized');
    }
}