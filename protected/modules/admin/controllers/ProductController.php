<?php

class ProductController extends AdminController
{
	/**
	 * Index method
	 */
	public function actionIndex()
	{
		$products = Product::model()->with('productType')->findAll();
		$this->render('index', array('products'=>$products));
	}

	/**
	 * Add a new product lot
	 */
	public function actionNewLot()
	{
		$model = new ProductLot;
		
		if(isset($_POST['ProductLot']))
		{
			$model->attributes = $_POST['ProductLot'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',' Product Lot have been saved');
				$this->redirect(array('index'));
			}
		}
		
		$this->render('create-lot', array('model'=>$model));
	}
	
	/**
	 * Add a new product
	 */
	public function actionNewProduct()
	{
		$model = new Product();
		if(isset($_POST['Product']))
		{
			$model->attributes = $_POST['Product'];
			$transaction = Yii::app()->db->beginTransaction();
			
			//Handle uploaded image file
			$rnd = rand(0,9999);
			$uploadedFile = CUploadedFile::getInstance($model,'image');
			$imageAttributes = getimagesize($uploadedFile->tempName);
			$width = $imageAttributes[0];	// first element is width of image
			$height = $imageAttributes[1];	// second element is height;
			
			//We would accept only a fixed dimension for images
			if($width!="300" && $height!="200")
			{
				Yii::app()->user->setFlash('error',' The image is not of appropriate width and height');
				$this->render('create-product', array('model'=>$model));
				return;
			}
			
			$filename = "{$rnd}-{$uploadedFile}";
			$model->image = $filename;

			if($model->save())
			{
				$uploadedFile->saveAs(Yii::app()->basepath.Yii::app()->params['productImagePath'].$filename);

				$transaction->commit();
					
				Yii::app()->user->setFlash('success',' Product has been saved');
				$this->redirect(array('index'));
			}
		}
		
		$this->render('create-product', array('model'=>$model));
	}
	
	public function actionUpdate($id)
	{
		$model = Product::model()->findByPk($id);
		if(isset($_POST['Product']))
		{
			$model->attributes = $_POST['Product'];
			$transaction = Yii::app()->db->beginTransaction();
				
			//Handle uploaded image file
			$rnd = rand(0,9999);
			$uploadedFile = CUploadedFile::getInstance($model,'image');
			$imageAttributes = getimagesize($uploadedFile->tempName);
			$width = $imageAttributes[0];	// first element is width of image
			$height = $imageAttributes[1];	// second element is height;
				
			//We would accept only a fixed dimension for images
			if($width!="300" && $height!="200")
			{
				Yii::app()->user->setFlash('error',' The image is not of appropriate width and height');
				$this->render('create-product', array('model'=>$model));
				return;
			}
				
			$filename = "{$rnd}-{$uploadedFile}";
			$model->image = $filename;
		
			if($model->save())
			{
				$uploadedFile->saveAs(Yii::app()->basepath.Yii::app()->params['productImagePath'].$filename);
		
				$transaction->commit();
					
				Yii::app()->user->setFlash('success',' Product has been saved');
				$this->redirect(array('index'));
			}
		}
		
		$this->render('create-product', array('model'=>$model));
	}
	
	public function actionNewType()
	{
		$model = new ProductType;
		$this->render('create-type', array('model'=>$model));
	}
	
	public function actionView()
	{
		$this->render('view');	
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}