<?php
/* @var $this CategoryController */
/* @var $model ProductCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->textField($model,'parent_id'); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'area_metrics'); ?>
		<?php echo $form->textField($model,'area_metrics',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'area_metrics'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'area_metrics_plural'); ?>
		<?php echo $form->textField($model,'area_metrics_plural',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'area_metrics_plural'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'area_metrics_abbr'); ?>
		<?php echo $form->textField($model,'area_metrics_abbr',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'area_metrics_abbr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'density'); ?>
		<?php echo $form->textField($model,'density',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'density'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'units_per_area'); ?>
		<?php echo $form->textField($model,'units_per_area',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'units_per_area'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->