<?php
/* @var $this CategoryController */
/* @var $data ProductCategory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area_metrics')); ?>:</b>
	<?php echo CHtml::encode($data->area_metrics); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area_metrics_plural')); ?>:</b>
	<?php echo CHtml::encode($data->area_metrics_plural); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area_metrics_abbr')); ?>:</b>
	<?php echo CHtml::encode($data->area_metrics_abbr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('density')); ?>:</b>
	<?php echo CHtml::encode($data->density); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('units_per_area')); ?>:</b>
	<?php echo CHtml::encode($data->units_per_area); ?>
	<br />

	*/ ?>

</div>