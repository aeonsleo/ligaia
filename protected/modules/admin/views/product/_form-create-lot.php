<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll'
	)
)); ?>	
	<div class="row">
		<div class="col-md-3">
			<b>Product Lot Info</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label>Choose Vendor for Product</label>
				<?php echo $form->dropDownList(
						$model,  
						'vendor_user_id',
						CHtml::listData(Vendor::model()->findAll(),
						'user_id',
						'name'),
						array('class'=>'form-control selectpicker', 'data-style'=>'btn-default')); ?> 
				<?php echo $form->error($model, 'vendor_user_id'); ?>
			</div>
			<div class="form-group">
				<label>Choose Product</label>
					<?php echo $form->dropDownList(
							$model,  
							'product_id',
							CHtml::listData(Product::model()->findAll(),
							'id',
							'name'),
							array('class'=>'form-control selectpicker', 'data-style'=>'btn-default')); ?> 
					<?php echo $form->error($model, 'product_id'); ?>
			</div>
			<div class="form-group">
				<label>Lot Serial no</label>
				<?php echo $form->textField($model, 'serial_no',  array('class'=>'form-control')); ?>
				<?php echo $form->error($model, 'serial_no'); ?>
			</div>
			<div class="form-group">
				<label>Units per lot</label>
				<?php echo $form->textField($model, 'units',  array('class'=>'form-control', 'value'=>1000)); ?>
				<?php echo $form->error($model, 'units'); ?>
			</div>			
			<div class="form-group">
				<label>Latitude Position</label>
				<?php echo $form->textField($model, 'latitude',  array('class'=>'form-control')); ?>
				<?php echo $form->error($model, 'latitude'); ?>
			</div>
			<div class="form-group">
				<label>Longitude Position</label>
				<?php echo $form->textField($model, 'longitude',  array('class'=>'form-control')); ?>
				<?php echo $form->error($model, 'longitude'); ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save changes</button>
				<a type="button" href="<?php echo Yii::app()->baseUrl?>/admin/product" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
			</div>			
		</div>
	</div>
<?php $this->endWidget(); ?>