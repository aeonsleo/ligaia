<!-- col-separator.box -->
<div class="col-separator bg-none col-unscrollable box col-separator-first">
	<!-- col-table -->
	<div class="col-table">
		<h4 class="innerAll margin-none bg-white">Product List</h4>
		<div class="col-separator-h">
		</div>
		<!-- col-table-row -->
		<div class="col-table-row">
			<!-- col-app -->
			<div class="col-app col-unscrollable">
				<!-- col-app -->
				<div class="col-app">
					<!-- Widget -->
					<div class="widget">
						<div class="widget-body innerAll inner-2x">
						<?php //Formatter::pretty($variants);?>
						<!-- Table -->
						<table class="dynamicTable tableTools table table-striped table-hover">
						<thead>
						<tr>
							<th>
								Product
							</th>
							<th>
								Category
							</th>
							<th>
								Type
							</th>
							<th>
								Variation
							</th>
							<th class="right">
								Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($products as $product):?>
						<tr class="gradeX">
							<td>
								<?php print $product->name; ?>
							</td>
							<td>
								<?php print $product->productType->productCategory->name;?>
							</td>
							<td>
								<?php print $product->productType->name ;?>
							</td>
							<td>
								<?php print $product->productVariation->name ?>
							</td>
							<td class="right">
								<div class="btn-group">
									<a href="<?php print Yii::app()->baseUrl?>/admin/product/view/id/<?php print $product->id ?>" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
									<a href="<?php print Yii::app()->baseUrl?>/admin/product/update/id/<?php print $product->id ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									<a href="<?php print Yii::app()->baseUrl?>/admin/product/delete/id/<?php print $product->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
						</tr>
						<?php endforeach;?>
						</tbody>
						</table>
						<!-- // Table END -->
		
						</div>
					</div>
					<!-- // Widget END -->
				</div>
				<!-- // END col-app -->
			</div>
			<!-- // END col-app.col-unscrollable -->
		</div>
		<!-- // END col-table-row -->
	</div>
	<!-- // END col-table -->
</div>
<!-- // END col-separator.box -->
