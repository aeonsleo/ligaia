<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll',
		'enctype'=>'multipart/form-data'
	)
)); ?>																
<div class="row">
	<div class="col-md-3">
		<b>Product profile</b>
		<p>
			Lorem ipsum dolor sit amet, consectetu adi
		</p>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="phone1">Product Name</label>
			<div class="input-group">
				<?php echo $form->textField($model, 'name',  array('class'=>'form-control', 'placeholder'=>'Teak 2 yr old')); ?>
			<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
			</div>
			<?php echo $form->error($model, 'name'); ?>
		</div>	

		<div class="form-group">
			<label for="type">Product Type</label>
			<?php echo $form->dropDownList(
					$model,  
					'product_type',
					CHtml::listData(ProductType::model()->findAll(),
					'name',
					'name'),
					array('class'=>'form-control selectpicker', 'data-style'=>'btn-default')); ?>
			<?php echo $form->error($model, 'product_type'); ?>
		</div>
		<div class="form-group">
			<label for="variation">Product Variation</label>
			<?php echo $form->dropDownList(
					$model,  
					'product_variation_id',
					CHtml::listData(ProductVariation::model()->findAll(),
					'id',
					'name'),
					array('class'=>'form-control selectpicker', 'data-style'=>'btn-default')); ?>
			<?php echo $form->error($model, 'product_variation_id'); ?>
		</div>
		<div class="form-group">
        		<label>Product Image</label>
        		<?php if(!empty($model->image)):?>
        		<div>
        			<img src="<?php print Yii::app()->baseUrl?>/images/product/<?php print $model->image?>" width="150"/>
        		</div>
        		<?php endif;?>
        		<p>Pleaes upload image with width: 300px, height: 200px</p>
        		<?php echo CHtml::activeFileField($model, 'image'); ?> 
        		<?php echo $form->error($model,'image'); ?>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save changes</button>
			<a type="button" href="<?php echo Yii::app()->baseUrl?>/admin/product" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
		</div>
	</div>
</div>
</div>
<?php $this->endWidget(); ?>