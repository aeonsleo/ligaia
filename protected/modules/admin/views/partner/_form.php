<?php
/* @var $this PartnerController */
/* @var $model Partner */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll'
	)
)); ?>
	<div class="row">
		<div class="col-md-3">
			<b>Partner profile</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="partnerName">Partner Name</label>
				<div class="input-group">
				<?php echo $form->textField($model,'partner_name',  array('class'=>'form-control', 'placeholder'=>'John Associates Pvt Ltd')); ?>
				<?php echo $form->error($model,'partner_name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="country">Country</label>
				<div class="input-group">
				<?php echo $form->dropDownList(
					$model,  
					'country_code',
					CHtml::listData(Geolocation::getCountries(),
					'code',
					'name'),
					array('empty'=>'Select Country')); ?> 
				</div>
				<?php echo $form->error($model,'country_code'); ?>
			</div>
			<div class="form-group">
				<label for="province">Province</label>
				<div class="input-group">
					<?php echo $form->textField($model,'province',  array('class'=>'form-control', 'placeholder'=>'Province')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'province'); ?>
			</div>
			<div class="form-group">
				<label for="address">Address</label>
				<div class="input-group">
					<?php echo $form->textField($model,'address_line_1',  array('class'=>'form-control', 'placeholder'=>'Address line 1')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'address_line_1'); ?>
			</div>
			<div class="form-group">
				<label for="addressLine2">Address line 2</label>
				<div class="input-group">
					<?php echo $form->textField($model,'address_line_2',  array('class'=>'form-control', 'placeholder'=>'Address line 2')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'address_line_2'); ?>
			</div>
			<div class="form-group">
				<label for="postalZipCode">Postal / Zip Code</label>
				<div class="input-group">
					<?php echo $form->textField($model,'zip',  array('class'=>'form-control', 'placeholder'=>'Postal/Zip Code')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'zip'); ?>
			</div>
			<div class="form-group">
				<label for="city">City</label>
				<div class="input-group">
					<?php echo $form->textField($model,'city',  array('class'=>'form-control', 'placeholder'=>'City')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'city'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Contact details profile</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<div class="input-group">
					<?php echo $form->textField($model,'firstname',  array('class'=>'form-control', 'placeholder'=>'John')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'first_name'); ?>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<div class="input-group">
					<?php echo $form->textField($model,'lastname',  array('class'=>'form-control', 'placeholder'=>'Doe')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'last_name'); ?>
			</div>
			<div class="form-group">
				<label for="phone">Phone</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'phone',  array('class'=>'form-control', 'placeholder'=>'0123456789')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'phone'); ?>
			</div>
			<div class="form-group">
				<label for="mobile">Mobile</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'mobile',  array('class'=>'form-control', 'placeholder'=>'01234567897')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'mobile'); ?>
			</div>
			<div class="form-group">
				<label for="fax">Fax</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'fax',  array('class'=>'form-control', 'placeholder'=>'01234567897')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'fax'); ?>
			</div>
			<div class="form-group">
				<label for="email">E-mail</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<?php echo $form->textField($userModel,'email',  array('class'=>'form-control', 'placeholder'=>'johndoe@yahoo.com')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($userModel,'email'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Partner details</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="organization">Organization</label>
				<?php echo $form->textField($model, 'organization_number',  array('class'=>'form-control', 'placeholder'=>'Organization')); ?>
				<?php echo $form->error($model, 'organization_number'); ?>
			</div>
			<div class="form-group">
				<label for="bankName">Name of Bank</label>
				<?php echo $form->textField($model, 'bank_name',  array('class'=>'form-control', 'placeholder'=>'Name of Bank')); ?>
				<?php echo $form->error($model, 'bank_name'); ?>
			</div>
			<div class="form-group">
				<label for="bankAccount">Bank Account number</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'bank_account_number',  array('class'=>'form-control', 'placeholder'=>'Bank Account Number')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'bank_account_number'); ?>
			</div>
			<div class="form-group">
				<label for="swift">Swift (BIC) number</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'bank_swift_code',  array('class'=>'form-control', 'placeholder'=>'Swift (BIC) Number')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'bank_swift_code'); ?>
			</div>
			<div class="form-group">
				<label for="iban">IBAN</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'iban',  array('class'=>'form-control', 'placeholder'=>'IBAN')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'iban'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Login details</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="organization">Username</label>
				<?php echo $form->textField($userModel, 'username',  array('class'=>'form-control', 'placeholder'=>'Username')); ?>
				<?php echo $form->error($userModel, 'username'); ?>
			</div>
			<div class="form-group">
				<label for="bankName">Password</label>
				<?php echo $form->passwordField($userModel, 'password',  array('class'=>'form-control', 'placeholder'=>'Password')); ?>
				<?php echo $form->error($userModel, 'password'); ?>
			</div>
			<div class="form-group">
				<label for="bankName">Password Repeat</label>
				<?php echo $form->passwordField($userModel, 'password_repeat',  array('class'=>'form-control', 'placeholder'=>'Password Repeat')); ?>
				<?php echo $form->error($userModel, 'password_repeat'); ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save changes</button>
				<a type="button" href="<?php echo Yii::app()->baseUrl?>/admin/product" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</div>	
<?php $this->endWidget(); ?>
