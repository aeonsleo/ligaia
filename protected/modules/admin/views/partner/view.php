<?php
/* @var $this PartnerController */
/* @var $model Partner */
?>
<div class="col-md-6">
	<div class="col-separator col-separator-first box hasNiceScroll">
		<div class="innerAll">
			<h4>Partner :  <?php echo $model->partner_name; ?></h4>
			<p>
				City: <?php echo $model->city?>	<br>
				Country: <?php echo $model->country_code?>							
			</p>
		</div>
	</div>
</div>
<div class="col-md-6 col-lg-6">
	<div class="col-separator col-separator-first box hasNiceScroll">
		<div class="innerAll">
			<div class="widget">
				<div class="widget-head">
					<h4 class="heading">Vendors</h4>
				</div>
				<div class="widget-body innerAll">
					<table class="table">
						<thead>
							<th>Name</th>
							<th>Country</th>
							<th>Region</th>
							<th>Lots</th>
						</thead>
						<?php foreach($model->vendors as $vendor):?>
						<tbody>
							<td><?php echo CHtml::link(CHtml::encode($vendor->name), array('/admin/vendor/view','id'=>$vendor->user_id))?></td>
							<td><?php echo $vendor->country_code?></td>
							<td><?php echo $vendor->province?></td>
							<td><?php echo $vendor->lotCount?></td>
						</tbody>
						<?php endforeach;?>
						</table>
				</div>
			</div>
		</div>
	</div>
</div>