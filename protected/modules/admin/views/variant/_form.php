<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll',
	)
)); ?>
	<div class="row">
		<div class="col-md-3">
			<b>Product Variant Info</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<div class="row">
					<div class="col-md-5">
						<label for="typeName">Type name</label>
						<?php echo $form->textField($model, 'name',  array('class'=>'form-control', 'placeholder'=>'Year')); ?>
						<?php echo $form->error($model, 'name'); ?>
					</div>
					<div class="col-md-5">
						<label for="plural">Plural</label>
						<?php echo $form->textField($model, 'name_plural',  array('class'=>'form-control', 'placeholder'=>'Years')); ?>
						<?php echo $form->error($model, 'name_plural'); ?>
					</div>
					<div class="col-md-2">
						<label for="categoryName">Abbr.</label>
						<?php echo $form->textField($model, 'name_abbr',  array('class'=>'form-control', 'placeholder'=>'yr')); ?>
						<?php echo $form->error($model, 'name_abbr'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Variant Value</label>
				<?php echo $form->textField($model, 'variation_value',  array('class'=>'form-control', 'placeholder'=>'1')); ?>
				<?php echo $form->error($model, 'variation_value'); ?>
			</div>
			<div class="form-group">
				<label for="Commercial">Commercial Volume (stem)</label>
				<?php echo $form->textField($model, 'commercial_volume',  array('class'=>'form-control', 'placeholder'=>'0.00')); ?>
				<?php echo $form->error($model, 'commercial_volume'); ?>
			</div>
		</div>
	</div>
	<div class="row">
			<div class="col-md-3">
				<b>BCEF</b>
				<p>
					Lorem ipsum dolor sit amet, consectetu adi
				</p>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="factor">Factor Conversion and Expantion Factor</label>
					<div class="input-group">
						<?php echo $form->textField($model, 'factor_conversion',  array('class'=>'form-control', 'placeholder'=>'9.00')); ?>
						<?php echo $form->error($model, 'factor_conversion'); ?>
						<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
					</div>
				</div>
				<div class="separator">
				</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save changes</button>
				<a type="button" href="<?php echo Yii::app()->baseUrl?>/admin/product" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</div>
<?php $this->endWidget(); ?>