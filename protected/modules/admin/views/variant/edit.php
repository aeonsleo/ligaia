<!-- col-separator.box -->
<div class="col-separator bg-none col-unscrollable box col-separator-first">
	<!-- col-table -->
	<div class="col-table">
		<h4 class="innerAll margin-none bg-white">Create/Edit Variant</h4>
		<div class="col-separator-h">
		</div>
		<!-- col-table-row -->
		<div class="col-table-row">
			<!-- col-app -->
			<div class="col-app col-unscrollable">
				<!-- col-app -->
				<div class="col-app">
					<!-- Widget -->
					<div class="widget">
						<div class="widget-body innerAll inner-2x">
							<?php $this->renderPartial('_form', array('model'=>$model)); ?>
						</div>
					</div>
					<!-- // Widget END -->
				</div>
				<!-- // END col-app -->
			</div>
			<!-- // END col-app.col-unscrollable -->
		</div>
		<!-- // END col-table-row -->
	</div>
	<!-- // END col-table -->
</div>
<!-- // END col-separator.box -->