<?php if(Yii::app()->user->hasFlash('success')):?>
	<div class="alert alert-success">
		<button data-dismiss="alert" class="close" type="button">x</button>
		<strong>Success!</strong><?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
<?php endif; ?>
<!-- col-separator.box -->
<div class="col-separator bg-none col-unscrollable box col-separator-first">
	<!-- col-table -->
	<div class="col-table">
		<h4 class="innerAll margin-none bg-white">Vendor engagement listing</h4>
		<div class="col-separator-h">
		</div>
		<!-- col-table-row -->
		<div class="col-table-row">
			<!-- col-app -->
			<div class="col-app col-unscrollable">
				<!-- col-app -->
				<div class="col-app">
					<!-- Widget -->
					<div class="widget">
						<div class="widget-body innerAll inner-2x">
							<!-- Table -->
							<table class="dynamicTable tableTools table table-striped table-hover">
							<!-- Table heading -->
							<thead>
							<tr>
								<th>
									Vendor
								</th>
								<th>
									Partner
								</th>
								<th>
									Country
								</th>
								<th>
									Parcels
								</th>
								<th>
									Total Value $
								</th>
								<th>
									Units Sold
								</th>
								<th>
									Units Total
								</th>
							</tr>
							</thead>
							<!-- // Table heading END -->
							<!-- Table body -->
							<tbody>
							<?php foreach($vendors as $vendor):?>
							<!-- Table row -->
							<tr class="gradeX">
								<td>
									<?php echo $vendor->name; ?>
								</td>
								<td>
									<?php echo $vendor->partner->partner_name; ?>
								</td>
								<td>
									<?php echo $vendor->country_code; ?>
								</td>
								<td>
									0
								</td>
								<td>
									0
								</td>
								<td>
									0
								</td>
								<td>
									0
								</td>
							</tr>
							<!-- // Table row END -->
							<?php endforeach; ?>
							</tbody>
							<!-- // Table body END -->
							</table>
							<!-- // Table END -->
						</div>
					</div>
					<!-- // Widget END -->
				</div>
				<!-- // END col-app -->
			</div>
			<!-- // END col-app.col-unscrollable -->
		</div>
		<!-- // END col-table-row -->
	</div>
	<!-- // END col-table -->
</div>
<!-- // END col-separator.box -->