<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vendor-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll'
	)
)); ?>
<form role="form">
	<div class="row">
		<div class="col-md-3">
			<b>Vendor profile</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="partner">Choose Partner for Vendor</label>
				<?php echo $form->dropDownList(
						$model,  
						'partner_id',
						CHtml::listData(Partner::model()->findAll(),
						'user_id',
						'partner_name'),
						array('empty'=>'Select Partner', 'class'=>'form-control selectpicker', 'data-style'=>'btn-default'));?>
				<?php echo $form->error($model, 'partner_id'); ?>
			</div>
			<div class="form-group">
				<label for="vendroName">Vendor Name (Finca/Plantation)</label>
				<?php echo $form->textField($model, 'name',  array('class'=>'form-control', 'placeholder'=>'Vendor Name (Finca/Plantation)')); ?>
				<?php echo $form->error($model, 'name'); ?>
			</div>
			<div class="form-group">
				<label for="country">Country</label>
				<?php echo $form->dropDownList(
					$model,  
					'country_code',
					CHtml::listData(Geolocation::getCountries(),
					'code',
					'name'),
					array('empty'=>'Select Country', 'class'=>'form-control selectpicker', 'data-style'=>'btn-default'));?> 
					<?php echo $form->error($model,'country_code'); ?>
			</div>
			<div class="form-group">
				<label for="province">Province</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'province',  array('class'=>'form-control', 'placeholder'=>'Province')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'province'); ?>
			</div>
			<div class="form-group">
				<label for="address">Address</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'address_line_1',  array('class'=>'form-control', 'placeholder'=>'Address line 1')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'address_line_1'); ?>
			</div>
			<div class="form-group">
				<label for="addressLine2">Address line 2</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'address_line_2',  array('class'=>'form-control', 'placeholder'=>'Address line 2')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'address_line_2'); ?>
			</div>
			<div class="form-group">
				<label for="postalZipCode">Postal / Zip Code</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'zip',  array('class'=>'form-control', 'placeholder'=>'Postal/Zip Code')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'zip'); ?>
			</div>
			<div class="form-group">
				<label for="city">City</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'city',  array('class'=>'form-control', 'placeholder'=>'City')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'city'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Owner Contact details</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<div class="input-group">
					<?php echo $form->textField($model,'firstname',  array('class'=>'form-control', 'placeholder'=>'John')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'first_name'); ?>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<div class="input-group">
					<?php echo $form->textField($model,'lastname',  array('class'=>'form-control', 'placeholder'=>'Doe')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'last_name'); ?>
			</div>
			<div class="form-group">
				<label for="phone">Phone</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'phone',  array('class'=>'form-control', 'placeholder'=>'0123456789')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'phone'); ?>
			</div>
			<div class="form-group">
				<label for="mobile">Mobile</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'mobile',  array('class'=>'form-control', 'placeholder'=>'01234567897')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'mobile'); ?>
			</div>
			<div class="form-group">
				<label for="fax">Fax</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<?php echo $form->textField($model,'fax',  array('class'=>'form-control', 'placeholder'=>'01234567897')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'fax'); ?>
			</div>
			<div class="form-group">
				<label for="email">E-mail</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<?php echo $form->textField($userModel,'email',  array('class'=>'form-control', 'placeholder'=>'johndoe@yahoo.com')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($userModel,'email'); ?>
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Vendor details</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="organization">Organization</label>
				<div class="input-group">
					<?php echo $form->textField($model, 'organisation_number',  array('class'=>'form-control', 'placeholder'=>'Organization')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'organisation_number'); ?>
			</div>
			<div class="form-group">
				<label for="established">Established</label>
				<div class="input-group">
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					    'model' => $model,
					    'attribute' => 'established',
					    'htmlOptions' => array(
					        'size' => '10',         // textField size
					        'maxlength' => '10',    // textField maxlength
							'class' => 'form-control'
					    ),
					));
					?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model, 'established'); ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save changes</button>
				<a type="button" href="<?php echo Yii::app()->baseUrl?>/admin/product" class="btn btn-default"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</div>
</form>
<?php $this->endWidget(); ?>
