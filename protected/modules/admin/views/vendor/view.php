<?php
/* @var $this VendorController */
/* @var $model Vendor */
?>

<h1>View Vendor #<?php echo $model->user_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'partner_id',
		'name',
		'organisation_number',
		'established',
		'address_line_1',
		'address_line_2',
		'address_line_3',
		'zip',
		'city',
		'phone',
		'mobile',
		'fax',
		'country_code',
	),
)); ?>
