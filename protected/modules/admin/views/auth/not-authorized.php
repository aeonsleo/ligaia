            <div class="container col-sm-6 col-sm-offset-3">
            <div class="row row-app">
                <!-- col -->
                <!-- col-separator.box -->
                <div class="col-separator col-unscrollable box">
                    <!-- col-table -->
                    <div class="col-table">
                        <h4 class="innerAll margin-none border-bottom text-center"><i class="fa fa-lock"></i> Unauthorized</h4>
                        <!-- col-table-row -->
                        <div class="col-table-row">
                            <!-- col-app -->
                            <div class="col-app col-unscrollable">
                                <!-- col-app -->
                                <div class="col-app">
                                    <div class="login">
                                        <div class="placeholder text-center">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <div class="panel panel-default col-sm-6 col-sm-offset-3">
                                            <div class="panel-body">
												<h5>You are not authorized to view this page</h5>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- // END col-app -->
                            </div>
                            <!-- // END col-app.col-unscrollable -->
                        </div>
                        <!-- // END col-table-row -->
                    </div>
                    <!-- // END col-table -->
                </div>
                <!-- // END col-separator.box -->
            </div>
            </div>    
         
        