            <div class="container col-sm-6 col-sm-offset-3">
            <div class="row row-app">
                <!-- col -->
                <!-- col-separator.box -->
                <div class="col-separator col-unscrollable box">
                    <!-- col-table -->
                    <div class="col-table">
                        <h4 class="innerAll margin-none border-bottom text-center"><i class="fa fa-lock"></i> Administrator Login</h4>
                        <!-- col-table-row -->
                        <div class="col-table-row">
                            <!-- col-app -->
                            <div class="col-app col-unscrollable">
                                <!-- col-app -->
                                <div class="col-app">
                                    <div class="login">
                                        <div class="placeholder text-center">
                                            <i class="fa fa-lock"></i>
                                        </div>
                                        <div class="panel panel-default col-sm-6 col-sm-offset-3">
                                            <div class="panel-body">
                                                <?php $form=$this->beginWidget('CActiveForm', array(
                                                    'id'=>'login-form',
                                                    'enableClientValidation'=>true,
                                                    'clientOptions'=>array(
                                                        'validateOnSubmit'=>true,
                                                    ),
                                                )); ?>

                                                    <div class="form-group">
                                                        <?php echo $form->labelEx($model,'username'); ?>
                                                        <?php echo $form->textField($model,'username', array('class'=>'form-control', 'placeholder'=>'username')); ?>
                                                        <?php echo $form->error($model,'username'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?php echo $form->labelEx($model,'password'); ?>
                                                        <?php echo $form->passwordField($model,'password', array('class'=>'form-control', 'placeholder'=>'password')); ?>
                                                        <?php echo $form->error($model,'password'); ?>
                                                    </div>
                                                    <?php echo CHtml::submitButton('Login', array('class'=>'btn btn-primary btn-block')); ?>

                                                    <div class="checkbox">
                                                        <?php echo $form->checkBox($model,'rememberMe'); ?>
                                                        <?php echo $form->label($model,'rememberMe'); ?>
                                                        <?php echo $form->error($model,'rememberMe'); ?>
                                                    </div>
                                                <?php $this->endWidget(); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-- // END col-app -->
                            </div>
                            <!-- // END col-app.col-unscrollable -->
                        </div>
                        <!-- // END col-table-row -->
                    </div>
                    <!-- // END col-table -->
                </div>
                <!-- // END col-separator.box -->
            </div>
            </div>    
         
