<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll'
	)
)); ?>
<div class="row">
	<div class="col-md-3">
		<b>Category profile</b>
		<p>
			Lorem ipsum dolor sit amet, consectetu adi
		</p>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="categoryName">Write Category name</label>
			<?php echo $form->textField($model,'name',  array('class'=>'form-control', 'placeholder'=>'Forrestry')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<label for="metrics">Area metrics</label>
					<?php echo $form->textField($model,'area_metrics',  array('class'=>'form-control', 'placeholder'=>'Acre')); ?>
					<?php echo $form->error($model,'area_metrics'); ?>
				</div>
				<div class="col-md-5">
					<label for="plural">Plural</label>
					<?php echo $form->textField($model,'area_metrics_plural',  array('class'=>'form-control', 'placeholder'=>'Acre')); ?>
					<?php echo $form->error($model,'area_metrics_plural'); ?>
				</div>
				<div class="col-md-2">
					<label for="categoryName">Abbr.</label>
					<?php echo $form->textField($model,'area_metrics_abbr',  array('class'=>'form-control', 'placeholder'=>'ha')); ?>
					<?php echo $form->error($model,'area_metrics_abbr'); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="Density">Density as default units per area</label>
			<?php echo $form->textField($model,'density',  array('class'=>'form-control', 'placeholder'=>'1000')); ?>
			<?php echo $form->error($model,'density'); ?>
		</div>
		<div class="form-group">
			<label for="Commercial">Commercial units per area</label>
			<?php echo $form->textField($model,'units_per_area',  array('class'=>'form-control', 'placeholder'=>'1000')); ?>
			<?php echo $form->error($model,'units_per_area'); ?>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
			<button type="button" class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>
