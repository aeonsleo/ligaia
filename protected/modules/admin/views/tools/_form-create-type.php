<form role="form">
	<div class="row">
		<div class="col-md-3">
			<b>Category profile</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="typeName">Write Type name</label>
				<input type="text" class="form-control" id="typeName" placeholder="Teak">
			</div>
			<div class="form-group">
				<label for="category">Choose Category</label>
				<select id="category" class="form-control">
					<option value="Forrestry">Forrestry</option>
				</select>
			</div>
			<div class="form-group">
				<label>Density factor if different from category</label>
				<div class="row">
					<div class="col-md-6">
						<label class="text-weight-normal" for="adjustmentType">Value adjustment to type</label>
						<div class="input-group">
							<input type="text" class="form-control" id="adjustmentType" placeholder="1000">
							<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
						</div>
					</div>
					<div class="col-md-6">
						<label class="text-weight-normal" for="defaultValue">Default value for category</label>
						<div class="input-group disabled">
							<input type="text" class="form-control" id="defaultValue" placeholder="1000" disabled="disabled">
							<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Morfic factors and ratios </b>
			<p>
				 Converts to the min value of Average of porcentages change between real and estimated values.
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="factor">Morfic factor</label>
				<div class="input-group">
					<input type="text" class="form-control" id="factor" placeholder="0,3758">
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label for="groundRatio">Above / Below Ground Ratio</label>
				<div class="input-group">
					<input type="text" class="form-control" id="groundRatio" placeholder="0,37">
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label for="biomass">Carbon content / Biomass</label>
				<div class="input-group">
					<input type="text" class="form-control" id="biomass" placeholder="0,47">
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label for="co2Ratio">CO2 / C ratio</label>
				<div class="input-group">
					<input type="text" class="form-control" id="co2Ratio" placeholder="0,366666">
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
				<button type="button" class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
			</div>
		</div>
	</div>
</form>