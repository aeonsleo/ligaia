	<div class="row tool-tables">
		<div class="col-md-6">
			<!-- col-separator.box -->
			<div class="bg-none col-unscrollable box">
				<!-- col-table -->
				<div class="col-table">
					<!-- col-table-row -->
					<div class="col-table-row">
						<!-- col-app -->
						<div class="col-app col-unscrollable">
							<!-- col-app -->
							<div class="col-app">
								<!-- Widget -->
								<div class="widget">
									<div class="widget-body innerAll inner-2x">
										<h5>
										Top 5 variation list <span class="pull-right">
										<a href="<?php echo Yii::app()->baseUrl?>/admin/variant" class="btn btn-xs btn-default">See all variations</a>
										<a href="<?php echo Yii::app()->baseUrl?>/admin/variant/new" class="btn btn-xs btn-success">Add variation</a>
										</span>
										</h5>
										<div class="separator">
										</div>
										<!-- Table -->
										<table class="tableTools table table-striped table-hover">
										<thead>
										<tr>
											<th>
												Variation
											</th>
											<th>
												QTY
											</th>
											<th>
												Commercial Volume
											</th>
											<th class="right">
												Action
											</th>
										</tr>
										</thead>
										<tbody>
											<?php foreach($variants as $variant):?>
											<tr class="gradeX">
												<td>
													<?php print $variant->name; ?>
												</td>
												<td>
													<?php print $variant->variation_value;?>
												</td>
												<td>
													<?php print $variant->commercial_volume;?>
												</td>
												<td class="right">
													<div class="btn-group">
														<a href="<?php print Yii::app()->baseUrl?>/admin/variant/view/<?php print $variant->id ?>" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
														<a href="<?php print Yii::app()->baseUrl?>/admin/variant/edit/<?php print $variant->id ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
														<a href="<?php print Yii::app()->baseUrl?>/admin/variant/delete/<?php print $variant->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
													</div>
												</td> 
											</tr>
											<?php endforeach;?>
										</tbody>
										</table>
										<!-- // Table END -->
										<p>
											<a href="<?php echo Yii::app()->baseUrl?>/admin/variant" class="text-success">See all</a> variations in the platform
										</p>
									</div>
								</div>
								<!-- // Widget END -->
							</div>
							<!-- // END col-app -->
						</div>
						<!-- // END col-app.col-unscrollable -->
					</div>
					<!-- // END col-table-row -->
					<div class="col-table-row">
						<!-- col-app -->
						<div class="col-app col-unscrollable">
							<!-- col-app -->
							<div class="col-app">
								<!-- Widget -->
								<div class="widget">
									<div class="widget-body innerAll inner-2x">
										<h5>
										Top 5 price trends <span class="pull-right">
										<a href="#" class="btn btn-xs btn-default">See all prices</a>
										<a href="#" class="btn btn-xs btn-success">Add price</a>
										</span>
										</h5>
										<div class="separator">
										</div>
										<!-- Table -->
										<table class="tableTools table table-striped table-hover">
										<!-- Table heading -->
										<thead>
										<tr>
											<th>
												Type
											</th>
											<th>
												Price
											</th>
											<th>
												$ pr.
											</th>
											<th>
												Trend
											</th>
											<th>
												Updated
											</th>
											<th class="right">
												Action
											</th>
										</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>

										</tbody>
										<!-- // Table body END -->
										</table>
										<!-- // Table END -->
										<p>
											<a href="#" class="text-success">See all</a> prices in the platform
										</p>
									</div>
								</div>
								<!-- // Widget END -->
							</div>
							<!-- // END col-app -->
						</div>
						<!-- // END col-app.col-unscrollable -->
					</div>
				</div>
				<!-- // END col-table -->
			</div>
			<!-- // END col-separator.box -->
		</div>
		<div class="col-md-6">
			<!-- col-separator.box -->
			<div class="bg-none col-unscrollable box">
				<!-- col-table -->
				<div class="col-table">
					<!-- col-table-row -->
					<div class="col-table-row">
						<!-- col-app -->
						<div class="col-app col-unscrollable">
							<!-- col-app -->
							<div class="col-app">
								<!-- Widget -->
								<div class="widget">
									<div class="widget-body innerAll inner-2x">
										<h5>
										Top 5 category list <span class="pull-right">
										<a href="<?php echo Yii::app()->baseUrl?>/admin/category" class="btn btn-xs btn-default">See all categories</a>
										<a href="#" class="btn btn-xs btn-success">Add category</a>
										</span>
										</h5>
										<div class="separator">
										</div>
										<!-- Table -->
										<table class="tableTools table table-striped table-hover">
										<!-- Table heading -->
										<thead>
											<tr>
												<th>
													Category
												</th>
												<th>
													Area Metrics
												</th>
												<th>
													Density
												</th>
												<th class="right">
													Action
												</th>
											</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
											<?php foreach($categories as $category):?>
											<tr class="gradeX">
												<td>
													<?php print $category->name; ?>
												</td>
												<td>
													<?php print $category->area_metrics;?>
												</td>
												<td>
													<?php print $category->density;?>
												</td>
												<td class="right">
													<div class="btn-group">
														<a href="<?php print Yii::app()->baseUrl?>/admin/category/view/<?php print $category->id ?>" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
														<a href="<?php print Yii::app()->baseUrl?>/admin/category/edit/<?php print $category->id ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
														<a href="<?php print Yii::app()->baseUrl?>/admin/category/edit/<?php print $category->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
													</div>
												</td>
											</tr>
											<?php endforeach;?>										
										</tbody>
										<!-- // Table body END -->
										</table>
										<!-- // Table END -->
										<p>
											<a href="<?php echo Yii::app()->baseUrl?>/admin/category" class="text-success">See all</a> categories in the platform
										</p>
									</div>
								</div>
								<!-- // Widget END -->
							</div>
							<!-- // END col-app -->
						</div>
						<!-- // END col-app.col-unscrollable -->
					</div>
					<div class="col-table-row">
						<!-- col-app -->
						<div class="col-app col-unscrollable">
							<!-- col-app -->
							<div class="col-app">
								<!-- Widget -->
								<div class="widget">
									<div class="widget-body innerAll inner-2x">
										<h5>
										Top 5 type list <span class="pull-right">
										<a href="<?php echo Yii::app()->baseUrl?>/admin/type" class="btn btn-xs btn-default">See all types</a>
										<a href="<?php echo Yii::app()->baseUrl?>/admin/type/create" class="btn btn-xs btn-success">Add type</a>
										</span>
										</h5>
										<div class="separator">
										</div>
										<!-- Table -->
										<table class="tableTools table table-striped table-hover">
										<!-- Table heading -->
										<thead>
										<tr>
											<th>
												Type
											</th>
											<th>
												Category
											</th>
											<th class="right">
												Action
											</th>
										</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
										<?php foreach($types as $type):?>
										<tr class="gradeX">
											<td>
												<?php print $type->name; ?>
											</td>
											<td>
												<?php print $type->productCategory->name;?>
											</td>
											<td class="right">
												<div class="btn-group">
													<a href="<?php print Yii::app()->baseUrl?>/admin/type/view/name/<?php print $type->name ?>" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
													<a href="<?php print Yii::app()->baseUrl?>/admin/type/update/name/<?php print $type->name ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
													<a href="<?php print Yii::app()->baseUrl?>/admin/type/delete/name/<?php print $type->name ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
												</div>
											</td>
										</tr>
										<?php endforeach;?>
										</tbody>
										<!-- // Table body END -->
										</table>
										<!-- // Table END -->
										<p>
											<a href="#" class="text-success">See all</a> types in the platform
										</p>
									</div>
								</div>
								<!-- // Widget END -->
							</div>
							<!-- // END col-app -->
						</div>
						<!-- // END col-app.col-unscrollable -->
					</div>
				</div>
				<!-- // END col-table -->
			</div>
			<!-- // END col-separator.box -->
		</div>
	</div>
