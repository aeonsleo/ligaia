<?php
/* @var $this TypeController */
/* @var $data ProductType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->name), array('view', 'id'=>$data->name)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('density')); ?>:</b>
	<?php echo CHtml::encode($data->density); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('morfic_factor')); ?>:</b>
	<?php echo CHtml::encode($data->morfic_factor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ab_ground_ratio')); ?>:</b>
	<?php echo CHtml::encode($data->ab_ground_ratio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carbon_content')); ?>:</b>
	<?php echo CHtml::encode($data->carbon_content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carbon_ratio')); ?>:</b>
	<?php echo CHtml::encode($data->carbon_ratio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->product_category_id); ?>
	<br />


</div>