<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'type-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal innerAll'
	)
)); ?>
	<div class="row">
		<div class="col-md-3">
			<b>Category profile</b>
			<p>
				Lorem ipsum dolor sit amet, consectetu adi
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="typeName">Write Type name</label>
				<?php echo $form->textField($model,'name',  array('class'=>'form-control', 'placeholder'=>'Teak')); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
			<div class="form-group">
				<label for="category">Choose Category</label>
				<?php echo $form->dropDownList($model,'product_category_id', CHtml::listData(ProductCategory::model()->findAll(), 'id', 'name'), array('empty'=>'Select Category', 'class'=>'form-control selectpicker')); ?>				
				<?php echo $form->error($model,'product_category_id'); ?>
			</div>
			<div class="form-group">
				<label>Density factor if different from category</label>
				<div class="row">
					<div class="col-md-6">
						<label class="text-weight-normal" for="adjustmentType">Value adjustment to type</label>
						<div class="input-group">
							<?php echo $form->textField($model,'density',  array('class'=>'form-control', 'placeholder'=>'1000', 'value'=>'1000')); ?>
							<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
						</div>
						<?php echo $form->error($model,'density'); ?>
					</div>
					<div class="col-md-6">
						<label class="text-weight-normal" for="defaultValue">Default value for category</label>
						<div class="input-group disabled">
							<input type="text" class="form-control" id="defaultValue" placeholder="1000" disabled="disabled">
							<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<b>Morfic factors and ratios </b>
			<p>
				 Converts to the min value of Average of porcentages change between real and estimated values.
			</p>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="factor">Morfic factor</label>
				<div class="input-group">
					<?php echo $form->textField($model,'morfic_factor',  array('class'=>'form-control', 'placeholder'=>'0.3758')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'morfic_factor'); ?>
			</div>
			<div class="form-group">
				<label for="groundRatio">Above / Below Ground Ratio</label>
				<div class="input-group">
					<?php echo $form->textField($model,'ab_ground_ratio',  array('class'=>'form-control', 'placeholder'=>'0.37')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'ab_ground_ratio'); ?>
			</div>
			<div class="form-group">
				<label for="biomass">Carbon content / Biomass</label>
				<div class="input-group">
					<?php echo $form->textField($model,'carbon_content',  array('class'=>'form-control', 'placeholder'=>'0.47')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'carbon_content'); ?>
			</div>
			<div class="form-group">
				<label for="co2Ratio">CO2 / C ratio</label>
				<div class="input-group">
					<?php echo $form->textField($model,'carbon_ratio',  array('class'=>'form-control', 'placeholder'=>'0.3666')); ?>
					<span class="input-group-addon" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet, consectetu adi" data-placement="bottom"><i class="fa fa-question-circle"></i></span>
				</div>
				<?php echo $form->error($model,'carbon_ratio'); ?>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save changes</button>
				<button type="button" class="btn btn-default"><i class="fa fa-times"></i> Cancel</button>
			</div>
		</div>
	</div>
<?php $this->endWidget(); ?>