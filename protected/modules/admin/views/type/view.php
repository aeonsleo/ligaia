<?php
/* @var $this TypeController */
/* @var $model ProductType */

$this->breadcrumbs=array(
	'Product Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ProductType', 'url'=>array('index')),
	array('label'=>'Create ProductType', 'url'=>array('create')),
	array('label'=>'Update ProductType', 'url'=>array('update', 'id'=>$model->name)),
	array('label'=>'Delete ProductType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->name),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductType', 'url'=>array('admin')),
);
?>

<h1>View ProductType #<?php echo $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'density',
		'morfic_factor',
		'ab_ground_ratio',
		'carbon_content',
		'carbon_ratio',
		'product_category_id',
	),
)); ?>
