<!-- col-separator.box -->
<div class="col-separator bg-none col-unscrollable box col-separator-first">
	<!-- col-table -->
	<div class="col-table">
		<h4 class="innerAll margin-none bg-white">Product Types</h4>
		<div class="col-separator-h">
		</div>
		<!-- col-table-row -->
		<div class="col-table-row">
			<!-- col-app -->
			<div class="col-app col-unscrollable">
				<!-- col-app -->
				<div class="col-app">
					<!-- Widget -->
					<div class="widget">
						<div class="widget-body innerAll inner-2x">
						<?php //Formatter::pretty($variants);?>
						<!-- Table -->
						<table class="tableTools table table-striped table-hover">
						<thead>
						<tr>
							<th>
								Type
							</th>
							<th>
								Category
							</th>
							<th>
								Density
							</th>
							<th>
								Morific Factor
							</th>
							<th>
								AB Ground Ratio
							</th>
							<th class="right">
								Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($types as $type):?>
						<tr class="gradeX">
							<td>
								<?php print $type->name; ?>
							</td>
							<td>
								<?php print $type->productCategory->name;?>
							</td>
							<td>
								<?php print $type->density;?>
							</td>
							<td>
								<?php print $type->morfic_factor;?>
							</td>
							<td>
								<?php print $type->ab_ground_ratio;?>
							</td>
							<td class="right">
								<div class="btn-group">
									<a href="<?php print Yii::app()->baseUrl?>/admin/type/view/name/<?php print $type->name ?>" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
									<a href="<?php print Yii::app()->baseUrl?>/admin/type/update/name/<?php print $type->name ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									<a href="<?php print Yii::app()->baseUrl?>/admin/type/delete/name/<?php print $type->name ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
						</tr>
						<?php endforeach;?>
						</tbody>
						</table>
						<!-- // Table END -->
		
						</div>
					</div>
					<!-- // Widget END -->
				</div>
				<!-- // END col-app -->
			</div>
			<!-- // END col-app.col-unscrollable -->
		</div>
		<!-- // END col-table-row -->
	</div>
	<!-- // END col-table -->
</div>
<!-- // END col-separator.box -->