<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 app footer-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 app footer-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 app footer-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie app footer-sticky"> <![endif]-->
<!--[if !IE]><!-->
<html class="app">
<!-- <![endif]-->
<head>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
-->
<link rel="stylesheet" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/css/admin/module.admin.stylesheet-complete.layout_fixed.true.css" /> 
<link rel="stylesheet" href="<?php echo $this->module->assetsUrl?>/css/admin.css" /> 

<!--[if lt IE 9]><link rel="stylesheet" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<?php 
	$cs = Yii::app()->getClientScript();
	$baseUrl = yii::app()->request->baseUrl;
	//$cs->registerCoreScript('jquery');
	$cs->registerScriptFile($baseUrl.'/coral/assets/components/library/jquery/jquery-migrate.min.js');
	$cs->registerScriptFile($baseUrl.'/coral/assets/components/plugins/less-js/less.min.js');
	$cs->registerScriptFile($baseUrl.'/coral/assets/components/library/modernizr/modernizr.js');
	$cs->registerScriptFile($baseUrl.'/coral/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js');
	$cs->registerScriptFile($baseUrl.'/coral/assets/components/plugins/browser/ie/ie.prototype.polyfill.js');
	
	$cs->registerCssFile($baseUrl.'/coral/assets/css/admin/module.admin.stylesheet-complete.sidebar_type.fusion.min.css');
	
?>
<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body class=" loginWrapper">
<!-- Main Container Fluid -->
<div class="container-fluid menu-hidden">
	<!-- Content -->
	<div id="content">
		<?php $this->widget('HeaderWidget')?>
        <?php echo $content ?>
    </div>
</div>
<!-- Global -->
<script data-id="App.Config">
var basePath = '',
    commonPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/',
    rootPath = '<?php echo yii::app()->request->baseUrl ?>/coral/',
    DEV = false,
    componentsPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/',
    layoutApp = true,
    module = 'admin';
var primaryColor = '#eb6a5a',
    dangerColor = '#b55151',
    successColor = '#609450',
    infoColor = '#4a8bc2',
    warningColor = '#ab7a4b',
    inverseColor = '#45484d';
var themerPrimaryColor = primaryColor;
</script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/breakpoints/breakpoints.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/core.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/animations.init.js?v=v1.9.6"></script>
</body>
</html>