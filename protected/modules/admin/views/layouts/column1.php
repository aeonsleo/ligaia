<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div id="content">
	<div class="col-md-12 animated fadeInDown">
	<?php echo $content; ?>
	</div>
</div><!-- content -->
<?php $this->endContent(); ?>