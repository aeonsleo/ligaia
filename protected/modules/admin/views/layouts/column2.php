<?php $this->beginContent('/layouts/admin'); ?>
<div class="container">
	<!-- row -->
	<div class="row row-app margin-none">
		<!-- col -->
		<div class="col-md-12">
			<!-- col-separator -->
			<div class="col-separator col-separator-first border-none">
				<!-- col-table -->
				<div class="col-table">
					<!-- col-table-row -->
					<div class="col-table-row">
						<!-- col-app -->
						<div class="col-app">
							<!-- row-app -->
							<div class="row row-app margin-none">
								<div class="col-md-3 animated fadeInDown">
									<div class="col-separator hasNiceScroll">
										<div class="widget" id="left-menu">
											<div class="innerAll inner-2x text-center">
												<a class="btn btn-lg btn-inverse btn-circle" href=""><i class="fa fa-fw fa-4x icon-collage"></i></a>
											</div>
											<div>
												<div class="relativeWrap">
													<div class="widget widget-tabs widget-tabs-responsive">
														<!-- Tabs Heading -->
														<div class="widget-head">
															<ul>
																<li class="tab-stacked active"><a data-toggle="tab" href="#tabPartners"><i></i><span>Partners</span><span class="badge badge-stroke">11</span></a></li>
																<li class="tab-stacked tab-inverse"><a data-toggle="tab" href="#tabVendors"><i></i><span>Vendors</span><span class="badge badge-stroke">77</span></a></li>
															</ul>
														</div>
														<!-- // Tabs Heading END -->
														<div class="widget-body">
															<div class="tab-content">
																<!-- Tab content -->
																<div class="tab-pane animated fadeInUp <?php if(Yii::app()->controller->id=='partner' || Yii::app()->controller->id=='tools') echo 'active'?>" id="tabPartners">
																	<div class="relativeWrap innerAll">
																		<p>
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie mi in nulla tempor aliquet. Nam vel lacus a elit egestas vestibulum eu tincidunt orci
																		</p>
																		<div class="widget widget-heading-simple">
																			<div class="text-center">
																				<p>
																					<a href="<?php echo yii::app()->baseUrl ?>/admin/partner/new" class="btn btn-success">Add Partner</a>
																				</p>
																				<p>
																					<a href="<?php echo yii::app()->baseUrl ?>/admin/vendor/new" class="btn btn-success">Add Vendor</a>
																				</p>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<p>
																				Pork loin. Doner pork loin porchetta rebeye pork belly kielbasa
																			</p>
																		</div>
																		<div class="clearfix">
																		</div>
																	</div>
																</div>
																<div class="tab-pane animated fadeInUp <?php if(Yii::app()->controller->id=='product' || Yii::app()->controller->id=='vendor') echo 'active'?>" id="tabVendors">
																	<div class="relativeWrap innerAll">
																		<p>
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed molestie mi in nulla tempor aliquet. Nam vel lacus a elit egestas vestibulum eu tincidunt orci
																		</p>
																		<div class="widget widget-heading-simple">
																			<div class="text-center">
																				<p>
																					<a href="<?php echo Yii::app()->baseUrl?>/admin/product/newproduct" class="btn btn-success">Add Product</a>
																				</p>
																				<p>
																					<a href="<?php echo Yii::app()->baseUrl?>/admin/product/newlot" class="btn btn-success">Add Product Lot</a>
																				</p>
																			</div>
																		</div>
																		<div class="col-md-12">
																			<p>
																				Pork loin. Doner pork loin porchetta rebeye pork belly kielbasa
																			</p>
																		</div>
																		<div class="clearfix">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="col-separator col-separator-first">
									<?php if(Yii::app()->user->hasFlash('success')):?>
										<div class="alert alert-success">
											<button data-dismiss="alert" class="close" type="button">x</button>
											<strong>Success!</strong><?php echo Yii::app()->user->getFlash('success'); ?>
										</div>
									<?php elseif(Yii::app()->user->hasFlash('error')):?>
										<div class="alert alert-danger">
											<button data-dismiss="alert" class="close" type="button">x</button>
											<strong>Error!</strong><?php echo Yii::app()->user->getFlash('error'); ?>
										</div>
									<?php endif; ?>				
									</div>				
									<?php echo $content;?>
								</div>
							</div>
							<!-- // END row -->
						</div>
						<!-- // END col-app -->
					</div>
					<!-- // END col-table-row -->
				</div>
				<!-- // END col-table -->
			</div>
			<!-- // END col-separator -->
		</div>
		<!-- // END col -->
	</div>
	<!-- // END row-app -->
</div>
<?php $this->endContent();?>