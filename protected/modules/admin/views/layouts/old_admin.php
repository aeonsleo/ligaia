<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if gt IE 8]> <html class="ie sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky"> <![endif]-->
<!--[if !IE]><!-->
<html class="app sidebar sidebar-fusion sidebar-kis footer-sticky navbar-sticky">
<!-- <![endif]-->
<head>
<title><?php echo CHtml::encode($this->
pageTitle); ?></title>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/less/admin/module.admin.stylesheet-complete.sidebar_type.fusion.less" />
	-->
<!--[if lt IE 9]><link rel="stylesheet" href="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
<link rel="stylesheet" href="<?php echo yii::app()->
request->baseUrl ?>/coral/assets/css/admin/module.admin.stylesheet-complete.sidebar_type.fusion.min.css" /> 
<link rel="stylesheet" href="<?php echo $this->module->assetsUrl?>/css/admin.css" /> 
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery/jquery.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery/jquery-migrate.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/modernizr/modernizr.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/less-js/less.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/jquery-ui/js/jquery-ui.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo $this->module->assetsUrl?>/js/admin.js""></script>
<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body class=" menu-right-hidden">
<!-- Main Container Fluid -->
<div class="container-fluid menu-hidden">
	<!-- Main Sidebar Menu -->
	<div id="menu" class="hidden-print hidden-xs sidebar-blue sidebar-brand-primary">
		<div id="sidebar-fusion-wrapper">
			<div id="brandWrapper">
				<a href="index.html?lang=en" class="display-block-inline pull-left logo"><img src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/images/logo/app-logo-style-default.png" alt=""></a>
				<a href="index.html?lang=en"><span class="text">Ligaia</span></a>
			</div>
			<?php 
			
			$this->widget('SideMenu', array(
			    'items'=>array(
			        // Important: you need to specify url as 'controller/action',
			        // not just as 'controller' even if default acion is used.
			        array('label'=>'Partners', 'url'=>'#partners', 'items'=>array(
						array('label'=>'List Partners', 'url'=>array('partner/index'), 'active'=>$this->uniqueid=='admin/partner' && $this->action->Id=='index'),
						array('label'=>'Add Partner', 'url'=>array('partner/create'), 'active'=>$this->uniqueid=='admin/partner' && $this->action->Id=='create'),
						),
						'linkOptions'=>array('data-toggle'=>'collapse', 'class'=>$this->uniqueid=='admin/partner'? 'collapsed': ''),
						'submenuOptions'=>array('id'=>'partners','class'=>$this->uniqueid=='admin/partner'? 'collapse in': 'collapse'),
						'active'=>$this->uniqueid=='admin/partner'
					),
			        // 'Products' menu item will be selected no matter which tag parameter value is since it's not specified.
			        array('label'=>'Vendors', 'url'=>'#vendors', 'items'=>array(
						array('label'=>'List Vendors', 'url'=>array('vendor/index'), 'active'=>$this->uniqueid=='admin/vendor' && $this->action->Id=='index'),
			            array('label'=>'Add Vendor', 'url'=>array('vendor/create'), 'active'=>$this->uniqueid=='admin/vendor' && $this->action->Id=='create'),
			        	),
						'linkOptions'=>array('data-toggle'=>'collapse', 'class'=>$this->uniqueid == 'admin/vendor'? 'collapsed': ''),
						'submenuOptions'=>array('id'=>'vendors', 'class'=>$this->uniqueid == 'admin/vendor'? 'collapse in': 'collapse'),
						'active'=>$this->uniqueid=='admin/vendor'
					),
					array('label'=>'Products', 'url'=>'#products', 'items'=>array(
						array('label'=>'List Products', 'url'=>array('product/index'), 'active'=>$this->uniqueid=='admin/product' && $this->action->Id=='index'),
						array('label'=>'Add Product', 'url'=>array('product/newProduct'), 'active'=>$this->uniqueid=='admin/product' && $this->action->Id=='newProduct'),
						array('label'=>'Add Product Variation', 'url'=>array('product/newVariant'), 'active'=>$this->uniqueid=='admin/product' && $this->action->Id=='newVariant'),
						array('label'=>'Add Product Lot', 'url'=>array('product/newLot'), 'active'=>$this->uniqueid=='admin/product' && $this->action->Id=='newLot'),
						),
						'linkOptions'=>array('data-toggle'=>'collapse', 'class'=>$this->uniqueid=='admin/product'? 'collapsed': ''),
						'submenuOptions'=>array('id'=>'products', 'class'=>$this->uniqueid=='admin/product'? 'collapse in': 'collapse'),
						'active'=>$this->uniqueid=='admin/product'
					),
			    ),
				'id'=>'mainNavigation',
				'activeCssClass'=>'active',
				'htmlOptions'=>array('class'=>'menu list-unstyled'),
				'itemCssClass'=>'hasSubmenu',
			));
			?>
		</div>
	</div>
	<!-- // Main Sidebar Menu END -->
	<!-- Content -->
	<div id="content">
		<div class="navbar hidden-print navbar-primary main" role="navigation">
			<div class="user-action user-action-btn-navbar pull-left border-right">
				<button class="btn btn-sm btn-navbar btn-primary btn-stroke"><i class="fa fa-bars fa-2x"></i></button>
			</div>
			<div class="user-action visible-xs user-action-btn-navbar pull-right">
				<button class="btn btn-sm btn-navbar-right btn-primary"><i class="fa fa-fw fa-arrow-right"></i><span class="menu-left-hidden-xs"> Modules</span></button>
			</div>
			<div class="user-action pull-right menu-right-hidden-xs menu-left-hidden-xs">
				<div class="dropdown username hidden-xs pull-left">
					<a class="dropdown-toggle " data-toggle="dropdown" href="#">
					<?php echo Yii::app()->user->username?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu pull-right">
						<li><a href="social_account.html?lang=en">Settings</a></li>
						<li><?php echo CHtml::link('Logout',array('auth/logout')); ?></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="layout-app" style="visibility: visible;">
			<!-- row-app -->
			<div class="row row-app lg-app">
				<!-- col -->
				<?php echo $content ?>
			</div>
		</div>
	</div>
	<!-- // Content END -->
	<div class="clearfix">
	</div>
	<!-- // Sidebar menu & content wrapper END -->
	<!-- Footer -->
	<div id="footer" class="hidden-print">
		<!--  Copyright Line -->
		<div class="copy">
			&copy; 2014 - <a href="http://ligaia.com">Ligaia</a> - All Rights Reserved.
		</div>
		<!--  End Copyright Line -->
	</div>
	<!-- // Footer END -->
</div>
<!-- // Main Container Fluid END -->
<!-- Global -->
<script data-id="App.Config">
	var basePath = '',
		commonPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/',
		layoutApp = false,
		module = 'admin';
	var primaryColor = '#eb6a5a',
		dangerColor = '#b55151',
		successColor = '#609450',
		infoColor = '#4a8bc2',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	var themerPrimaryColor = primaryColor;
	</script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/plugins/breakpoints/breakpoints.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/custom/js/datatables.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/common/tables/classic/assets/js/tables-classic.init.js?v=v1.9.6&sv=v0.0.1"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/sidebar.main.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/sidebar.collapse.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/core.init.js?v=v1.9.6"></script>
<script src="<?php echo yii::app()->request->baseUrl ?>/coral/assets/components/core/js/animations.init.js?v=v1.9.6"></script>
</body>
</html>