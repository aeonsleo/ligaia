<?php

/**
 * Base class for all admin controllers except the auth controller
 * 
 * @author Abhishek Srivastava <abhishek.sr017@gmail.com>
 *
 */
class AdminController extends Controller
{
	public function __construct($id, $module=null)
	{
		parent::__construct($id, $module);
		
		/* Check if the user is logged in */
		if(!Yii::app()->user->getId())
		{
			$this->redirect(array('auth/login'));	
		}
		/* If the user is logged in then check if the user has superadmin role */
		elseif(!Yii::app()->user->checkAccess('superadmin'))
		{
			$this->redirect(array('auth/unauthorized'));
		}
	}
}