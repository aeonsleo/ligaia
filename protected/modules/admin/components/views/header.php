		<div class="navbar navbar-inverse hidden-print main navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="logo" href="<?php echo Yii::app()->baseUrl?>/admin/dashboard" class="animated fadeInDown pull-left"><img src="<?php echo Yii::app()->baseUrl?>/images/admin/logo.png" alt="" class="img-clean"></a>
				</div>
				<div class="navbar-collapse collapse">
				<?php if(Yii::app()->user->getId() && Yii::app()->user->checkAccess('superadmin')):?>
					<?php $this->widget('zii.widgets.CMenu', array(
						'items'=>array(
							array(
								'label' => '<i class="fa fa-fw icon-graph-up-1"></i> Dashboard</a>', 
								'url'=>array('dashboard/index'),
								'active'=>Yii::app()->controller->id=='dashboard'
							),
							array(
								'label'=>'<i class="fa fa-fw icon-cogs"></i> Portfolio</a>', 
								'url'=>array('portfolio/index'),
								'active'=>Yii::app()->controller->id=='portfolio'
							),
							array(
								'label'=>'<i class="fa fa-fw icon-collage"></i> Partners</a>', 
								'url'=>array('partner/index'),
								'active'=>Yii::app()->controller->id=='partner'||Yii::app()->controller->id=='vendor'
							),
							array(
								'label'=>'<i class="fa fa-fw icon-wrench"></i> Tools</a>', 
								'url'=>array('tools/index'),
								'active'=>Yii::app()->controller->id=='tools',
								'visible'=>'true',
								'items'=>array(
									array(
										'label'=>'New Category',
										'url'=>array('tools/newcategory'),
										'active'=>Yii::app()->controller->id=='tools' && Yii::app()->controller->action=='newcategory',
										'visible'=>false
									),
								),
							),
						),
						'encodeLabel' => false,
						'htmlOptions' => array(
							'class'=>'nav navbar-nav main-menu lig-top-menu',
						),
						'submenuHtmlOptions' => array(
							'class' => '',
						)
					)); ?>	
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown dropdown-light">
						<a href="" data-toggle="dropdown" class="dropdown-toggle"><?php print($user->username)?><span class="caret"></span><i class="fa fa-fw icon-user-1 user-icon"></i></a>
						<ul class="dropdown-menu pull-left">
							<li><a href="#" class="glyphicons shopping_cart"><i></i><span>Settings</span></a></li>
							<li><a href="<?php echo Yii::app()->baseUrl?>/admin/auth/logout" class="glyphicons home"><i></i><span>Logout</span></a></li>
						</ul>
						</li>
					</ul>
					<?php endif;?>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>