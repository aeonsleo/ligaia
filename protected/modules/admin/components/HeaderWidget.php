<?php

/**
 * Header Widget Class : Responsible to display the header data e.g. logo, menu and user info
 * @author Abhishek <abhishek.sr017@gmail.com>
 *
 */
class HeaderWidget extends CWidget {
	public function run() {
		$userId = Yii::app()->user->getId();
		$user = User::model()->findByPk($userId);
		
		$this->render('header', array(
				'user'=>$user
		));
	}
}