<?php

abstract class LigaiaActiveRecord extends CActiveRecord
{
	/**
	 * Attaches the timestamp behavior to update our create and update times
	 */
	public function behaviors()
	{
		return array(
				'CTimestampBehavior' => array(
						'class' => 'zii.behaviors.CTimestampBehavior',
						'createAttribute' => 'created',
						'updateAttribute' => 'updated',
						'setUpdateOnCreate' => true,
				),
		);
	}
}