<?php

class UserController extends Controller
{
	public function actionIndex()
	{
	}
	
	public function actionProfile()
	{
		if(!Yii::app()->user->checkAccess('investor'))
		{
			$this->redirect(array('user/login'));
		}
		
		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array(
					'condition'=>'id='.Yii::app()->user->getId(),
					'with'=>array('investor'),
			),
		));

		$user = User::model()->findByPk(Yii::app()->user->getId());
		$investorModel = $user->investor;
		//Formatter::pretty($user);exit;
		$users = $dataProvider->getData();
		
		
		if(isset($_POST['Investor']))
		{
			$user->investor->attributes = $_POST['Investor'];
			
			$investorModel->attributes = $_POST['Investor'];

			if($investorModel->save())
			{
				Yii::app()->user->setFlash('success', ' User profile updated');
			}
			else 
			{
				Yii::app()->user->setFlash('error', ' There were errors! User profile could not be updated');
			}
		}
		
		if(isset($_POST['User']['new_password']))
		{
			$user->attributes = $_POST['User'];
			
			if($user->save())
			{
				Yii::app()->user->setFlash('success', ' User profile updated');
			}
			else 
			{
				Yii::app()->user->setFlash('error', ' There were errors! User profile could not be updated');
			}
			//Formatter::pretty($user);exit;
		}
		
		$this->render('profile',array(
				'user'=>$user, 
				'investorModel'=>$investorModel
		));
	}

	public function actionupdateProfile()
	{

	}
	
	public function actionRegister()
	{
		if(Yii::app()->user->getId())
			$this->redirect(array('/dashboard'));
		
        $this->layout = 'login';
        
		$userModel = new User;
        $investorModel =new Investor;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($userModel);
        foreach(Yii::app()->user->getFlashes() as $key => $message) {
            echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
        }
        
        if(isset($_POST['User']))
		{
			$userModel->attributes = $_POST['User'];
            $criteria = new CDbCriteria;
            $criteria->condition = "status_type = 'user' AND status = 'unverified'";
            $status = Status::model()->find($criteria);
            $userModel->status_id = $status->id;
			if($userModel->save() && isset($_POST['Investor']))
            {
                //Save investor information
                $userId = $userModel->getPrimaryKey();
                $investorModel->attributes = $_POST['Investor'];
                $investorModel->user_id = $userId;
                $investorModel->email = $userModel->email;
                
                if($investorModel->save())
                {
                    //Send activation email
                    $activationUrl = $this->createAbsoluteUrl('user/verify', array('key' => $investorModel->activation_key));
                    $this->sendUserVerificationMail($activationUrl, $investorModel);
                    $this->render('register-success');
                }
            }
		}
        else 
        {
            $this->render('registerInvestor',array(
                'userModel'=> $userModel,
                'investorModel' => $investorModel
            ));
        }
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if(Yii::app()->user->getId())
			$this->redirect(array('/dashboard'));
		
        $this->layout="login";
        
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
    
    public function sendUserVerificationMail($activationUrl, $investorModel)
    {
        $emailTemplate = EmailTemplate::model()->find('name=:name', array(':name' => 'user_verification'));
        //$emailMessage = $emailTemplate->template;
        $emailMessage = str_replace("[%verification_link%]", $activationUrl, $emailTemplate->template);
        
        $mail = new YiiMailer('user_verification', array('message' => $emailMessage, 'name' => $investorModel->firstname, 'description' => 'User Verification'));

        $mail->setFrom($emailTemplate->from_email, $emailTemplate->from_name);
        $mail->setTo($investorModel->email, $investorModel->firstname.' '.$investorModel->lastname);
        $mail->setSubject($emailTemplate->subject);
        
        if ($mail->send()) 
        {
            Yii::app()->user->setFlash('message','Thank you for contacting us. We will respond to you as soon as possible.');
        } 
        else 
        {
            Yii::app()->user->setFlash('message','Error while sending email: '.$mail->getError());
        }       
    }

    public function actionVerify()
    {
    	$this->layout = 'register';
    	
        if(!isset($_GET['key']))
        {
            echo 'Key not provided';
        }
        
        $key = $_GET['key'];
        
        $investorModel = Investor::Model();
        
        //Check if the user is already verified
        if($investorModel->isVerified($key))
        {
        	//Investor is already verified
        	$this->render('already-verified');
        	return;
        }
        
        if(!$investor = $investorModel->verify($key))
        {
            //User not verified. Remind that there is some issue.
            $this->render('verification-failure');
        	return;
        }
        
        //User verified, change user status and let to login
        $criteria = new CDbCriteria;
        $criteria->condition = "status_type = 'user' AND status = 'verified'";
        $status = Status::model()->find($criteria);
        $user = $investor->user;
        $user->status_id = $status->id;
        if($user->save())
        {
          	//User verification successful
          	$this->render('verification-success');
        }
        else
        {
          	//User verification unsuccessful
 	    }
    }
    
    public function actionVerification()
    {
    	$this->layout = 'register';
    	$model = new VerificationForm();
    	if(isset($_POST['VerificationForm']))
    	{
    		$model->attributes = $_POST['VerificationForm'];
    		if($model->validate() && $model->sendVerificationEmail())
    		{
    			$user = User::model()->find('email=:email', array(':email'=>$model->email));
    			if(!$user) 
    			{
    				//Investor email does not exist
    				//Set flash message
    				echo 'Email does not exist';
    			}
    			$investor = $user->investor;
    			$investor->generateActivationKey();
    			if(!$investor->save())
    			{
    				//New activation key could not be saved
    				return;
    			}
    			$activationUrl = $this->createAbsoluteUrl('user/verify', array('key' => $investor->activation_key));
    			$this->sendUserVerificationMail($activationUrl, $investor);
    			$this->render('verification-email-sent');
    		}
    	}
    	$this->render('send-verification-email', array('model'=>$model));
    }
    
    public function actionLogout()
    {
    	Yii::app()->user->logout(false);
    	$this->redirect(array('/user/login'));
    }
    
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}