<?php

class m140216_123519_create_rbac_tables extends CDbMigration
{
	public function up()
	{
		//create the auth item table
		$this->createTable('lig_auth_item', array(
				'name' =>'varchar(64) NOT NULL',
				'type' =>'integer NOT NULL',
				'description' =>'text',
				'bizrule' =>'text',
				'data' =>'text',
				'PRIMARY KEY (`name`)',
		), 'ENGINE=InnoDB');
		
		//create the auth item child table
		$this->createTable('lig_auth_item_child', array(
				'parent' =>'varchar(64) NOT NULL',
				'child' =>'varchar(64) NOT NULL',
				'PRIMARY KEY (`parent`,`child`)',
		), 'ENGINE=InnoDB');
		
		//the lig_auth_item_child.parent is a reference to lig_auth_item.name
		$this->addForeignKey("fk_auth_item_child_parent", "lig_auth_item_child", 
				"parent", "lig_auth_item", "name", "CASCADE", "CASCADE");
		
		//the lig_auth_item_child.child is a reference to lig_auth_item.name
		$this->addForeignKey("fk_auth_item_child_child", "lig_auth_item_child", 
				"child", "lig_auth_item", "name", "CASCADE", "CASCADE");
		
		//create the auth assignment table
		$this->createTable('lig_auth_assignment', array(
				'itemname' =>'varchar(64) NOT NULL',
				'userid' =>'int(11) NOT NULL',
				'bizrule' =>'text',
				'data' =>'text',
				'PRIMARY KEY (`itemname`,`userid`)',
		), 'ENGINE=InnoDB');
		
		//the lig_auth_assignment.itemname is a reference
		//to lig_auth_item.name
		$this->addForeignKey(
				"fk_auth_assignment_itemname",
				"lig_auth_assignment",
				"itemname",
				"lig_auth_item",
				"name",
				"CASCADE",
				"CASCADE"
		);
		
		//the lig_auth_assignment.userid is a reference
		//to lig_user.id
		$this->addForeignKey(
				"fk_auth_assignment_userid",
				"lig_auth_assignment",
				"userid",
				"lig_user",
				"id",
				"CASCADE",
				"CASCADE"
		);
	}

	public function down()
	{
		$this->truncateTable('lig_auth_assignment');
		$this->truncateTable('lig_auth_item_child');
		$this->truncateTable('lig_auth_item');
		$this->dropTable('lig_auth_assignment');
		$this->dropTable('lig_auth_item_child');
		$this->dropTable('lig_auth_item');
	}

	/*
	 // Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
	
}