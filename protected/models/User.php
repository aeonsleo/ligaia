<?php

/**
 * This is the model class for table "lig_user".
 *
 * The followings are the available columns in table 'lig_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $last_login
 * @property string $created
 * @property string $updated
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Auction[] $auctions
 * @property AuthItem[] $ligAuthItems
 * @property Bid[] $bs
 * @property Group[] $ligGroups
 * @property Investor $investor
 * @property LotUser[] $lotUsers
 * @property Offering[] $offerings
 * @property Partner $partner
 * @property ShoppingCart[] $shoppingCarts
 * @property Status $statusModel
 * @property Vendor $vendor
 */
class User extends LigaiaActiveRecord
{
	public $current_password;
	public $new_password;
    public $password_repeat;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('current_password', 'compareCurrentPassword'),
			array('username, password, password_repeat, email', 'required', 'on'=>'create'),
			array('new_password, current_password, password_repeat', 'required', 'on'=>'update'),
			array('username, password, current_password, new_password, password_repeat', 'length', 'max'=>50),
			array('email', 'length', 'max'=>100),
			array('address, password_repeat', 'safe'),
			array('email, username', 'unique'),
			array('email', 'email'),	
			array('password', 'compare', 'on'=>'create'),
			array('password', 'compareNewPassword', 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('username, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'auctions' => array(self::HAS_MANY, 'Auction', 'created_by'),
			'ligAuthItems' => array(self::MANY_MANY, 'AuthItem', 'lig_auth_assignment(userid, itemname)'),
			'bs' => array(self::HAS_MANY, 'Bid', 'user_id'),
			'ligGroups' => array(self::MANY_MANY, 'Group', 'lig_group_member(user_id, group_id)'),
			'investor' => array(self::HAS_ONE, 'Investor', 'user_id'),
			'lotUsers' => array(self::HAS_MANY, 'LotUser', 'user_id'),
			'offerings' => array(self::HAS_MANY, 'Offering', 'user_id'),
			'partner' => array(self::HAS_ONE, 'Partner', 'user_id'),
			'shoppingCarts' => array(self::HAS_MANY, 'ShoppingCart', 'user_id'),
			'statusModel' => array(self::BELONGS_TO, 'Status', 'status_id'),
			'vendor' => array(self::HAS_ONE, 'Vendor', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'password_repeat' => 'Repeat Password',
			'current_password' => 'Current Password',
			'email' => 'Email',
			'last_login' => 'Last Login',
			'created' => 'Created',
			'updated' => 'Updated',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password)===$this->password;
	}
    
	/**
	 * apply a hash on the password before we store it in the database
	 */
	protected function afterValidate()
	{
		parent::afterValidate();
		if(!$this->hasErrors())
		{
			if($this->scenario==="insert")
			{
				$this->password = $this->hashPassword($this->password);
			}
			elseif($this->scenario==="update")
			{
				$this->password = $this->hashPassword($this->new_password);
			}
		}
			
	}
	
	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return md5($password);
	}
	
	public function compareCurrentPassword()
	{
		if(md5($this->current_password) != $this->password)
		{
			$this->addError('current_password', 'Current password is not correct');
		}
	}

	public function compareNewPassword()
	{
		if($this->new_password != $this->password_repeat)
		{
			$this->addError('new_password', 'Passwords do not match');
			$this->addError('password_repeat', 'Passwords do not match');
		}
	}
	
	public function changePassword()
	{
		$this->_user->password = $this->password;
		if( $this->_user->save() )
			return true;
		return false;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
