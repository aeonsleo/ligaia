<?php

class VerificationForm extends CFormModel
{
	public $email;
	
	public function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'email')
		);
	}
	
	/**
	* Declares attribute labels.
	*/
	public function attributeLabels()
	{
		return array(
				'email'=>'Email',
		);
	}
	
	public function sendVerificationEmail()
	{
		
		return true;
	}
}