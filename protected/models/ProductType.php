<?php

/**
 * This is the model class for table "lig_product_type".
 *
 * The followings are the available columns in table 'lig_product_type':
 * @property string $name
 * @property integer $density
 * @property double $morfic_factor
 * @property double $ab_ground_ratio
 * @property double $carbon_content
 * @property double $carbon_ratio
 * @property integer $product_category_id
 *
 * The followings are the available model relations:
 * @property Product[] $products
 * @property ProductCategory $productCategory
 */
class ProductType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_product_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, density, morfic_factor, ab_ground_ratio, carbon_content, carbon_ratio, product_category_id', 'required'),
			array('density, product_category_id', 'numerical', 'integerOnly'=>true),
			array('morfic_factor, ab_ground_ratio, carbon_content, carbon_ratio', 'numerical'),
			array('name', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, density, morfic_factor, ab_ground_ratio, carbon_content, carbon_ratio, product_category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'product_type'),
			'productCategory' => array(self::BELONGS_TO, 'ProductCategory', 'product_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Name',
			'density' => 'Density Factor',
			'morfic_factor' => 'Morfic Factor',
			'ab_ground_ratio' => 'Above/Below Ground Ratio',
			'carbon_content' => 'Carbon Content/Biomass',
			'carbon_ratio' => 'CO2/C Ratio',
			'product_category_id' => 'Product Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('density',$this->density);
		$criteria->compare('morfic_factor',$this->morfic_factor);
		$criteria->compare('ab_ground_ratio',$this->ab_ground_ratio);
		$criteria->compare('carbon_content',$this->carbon_content);
		$criteria->compare('carbon_ratio',$this->carbon_ratio);
		$criteria->compare('product_category_id',$this->product_category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
