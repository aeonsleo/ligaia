<?php

/**
 * This is the model class for table "lig_partner".
 *
 * The followings are the available columns in table 'lig_partner':
 * @property integer $user_id
 * @property string $partner_name
 * @property string $firstname
 * @property string $lastname
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $address_line_3
 * @property string $zip
 * @property string $city
 * @property string $country_code
 * @property string $province
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $organization_number
 * @property string $bank_name
 * @property string $bank_account_number
 * @property string $bank_swift_code
 * @property string $iban
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Country $country
 * @property Vendor[] $vendors
 */
class Partner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, partner_name, firstname, lastname, city, country_code, province, phone, organization_number, bank_name, bank_account_number, bank_swift_code, iban', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('partner_name, firstname, lastname, address_line_1, address_line_2, address_line_3, province, bank_name', 'length', 'max'=>100),
			array('zip, city, country_code, phone, mobile, fax, organization_number, bank_account_number, bank_swift_code, iban', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, partner_name, firstname, lastname, address_line_1, address_line_2, address_line_3, zip, city, country_code, province, phone, mobile, fax, organization_number, bank_name, bank_account_number, bank_swift_code, iban', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'country' => array(self::BELONGS_TO, 'Country', 'country_code'),
			'vendors' => array(self::HAS_MANY, 'Vendor', 'partner_id'),
			'vendor_count' => array(self::STAT, 'Vendor', 'partner_id'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'partner_name' => 'Partner Name',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'address_line_1' => 'Address Line 1',
			'address_line_2' => 'Address Line 2',
			'address_line_3' => 'Address Line 3',
			'zip' => 'Zip',
			'city' => 'City',
			'country_code' => 'Country Code',
			'province' => 'Province',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'fax' => 'Fax',
			'organization_number' => 'Organization Number',
			'bank_name' => 'Bank Name',
			'bank_account_number' => 'Bank Account Number',
			'bank_swift_code' => 'Bank Swift Code',
			'iban' => 'Iban',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('partner_name',$this->partner_name,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('address_line_1',$this->address_line_1,true);
		$criteria->compare('address_line_2',$this->address_line_2,true);
		$criteria->compare('address_line_3',$this->address_line_3,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('organization_number',$this->organization_number,true);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('bank_account_number',$this->bank_account_number,true);
		$criteria->compare('bank_swift_code',$this->bank_swift_code,true);
		$criteria->compare('iban',$this->iban,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
