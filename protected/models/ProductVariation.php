<?php

/**
 * This is the model class for table "lig_product_variation".
 *
 * The followings are the available columns in table 'lig_product_variation':
 * @property integer $id
 * @property string $name
 * @property integer $variation_value
 * @property string $name_plural
 * @property string $name_abbr
 * @property double $commercial_volume
 * @property double $factor_conversion
 */
class ProductVariation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_product_variation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, variation_value, name_plural, name_abbr, commercial_volume, factor_conversion', 'required'),
			array('variation_value', 'numerical', 'integerOnly'=>true),
			array('commercial_volume, factor_conversion', 'numerical'),
			array('name, name_plural, name_abbr', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, variation_value, name_plural, name_abbr, commercial_volume, factor_conversion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'variation_value' => 'Variation Value',
			'name_plural' => 'Name Plural',
			'name_abbr' => 'Name Abbr',
			'commercial_volume' => 'Commercial Volume',
			'factor_conversion' => 'Factor Conversion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('variation_value',$this->variation_value);
		$criteria->compare('name_plural',$this->name_plural,true);
		$criteria->compare('name_abbr',$this->name_abbr,true);
		$criteria->compare('commercial_volume',$this->commercial_volume);
		$criteria->compare('factor_conversion',$this->factor_conversion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductVariation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
