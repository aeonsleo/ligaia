<?php

/**
 * This is the model class for table "lig_investor".
 *
 * The followings are the available columns in table 'lig_investor':
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $address_line_3
 * @property string $zip
 * @property string $city
 * @property string $region
 * @property string $country
 * @property string $phone
 * @property string $dob
 * @property string $activation_key
 *
 * The followings are the available model relations:
 * @property User $user
 * @property City $country0
 * @property Invoice[] $invoices
 */
class Investor extends CActiveRecord
{
	public $email;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_investor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, firstname, lastname, company, city, region, country', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, address_line_1, address_line_2, address_line_3, activation_key', 'length', 'max'=>100),
			array('company, zip, city, region, country, phone, dob', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, firstname, lastname, company, address_line_1, address_line_2, address_line_3, zip, city, region, country, phone, activation_key', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'country0' => array(self::BELONGS_TO, 'City', 'country'),
			'invoices' => array(self::HAS_MANY, 'Invoice', 'investor_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'company' => 'Company',
			'address_line_1' => 'Address Line 1',
			'address_line_2' => 'Address Line 2',
			'address_line_3' => 'Address Line 3',
			'zip' => 'Zip',
			'city' => 'City',
			'region' => 'Region',
			'country' => 'Country',
			'phone' => 'Phone',
			'dob' => 'Date of Birth',
			'activation_key' => 'Activation Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('address_line_1',$this->address_line_1,true);
		$criteria->compare('address_line_2',$this->address_line_2,true);
		$criteria->compare('address_line_3',$this->address_line_3,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('region',$this->region,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('activation_key',$this->activation_key,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		parent::beforeSave();
		$this->generateActivationKey();
		return true;
	}
	
	public function generateActivationKey()
	{
		$this->activation_key = sha1(mt_rand(10000, 99999).time().$this->email);
	}
	
	public function isVerified($key)
	{
		$investor = Investor::model()->with(array('user.statusModel'))->find('activation_key=:key', array(':key'=>$key));
		if($investor && $investor->user->statusModel->status == 'verified')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function verify($key)
	{
		$user = $this->with('user')->find('activation_key=:key', array(':key'=>$key));
	
		if($user)
		{
			return $user;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Investor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
