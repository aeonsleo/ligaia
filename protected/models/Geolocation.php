<?php

abstract class Geolocation extends LigaiaActiveRecord
{

    public static function getCountries()
    {
        $countries_data_reader = Yii::app()->db->createCommand()
            ->select('code, name')
            ->from('country')
            ->order('name')
            ->query();
            
        $countries = $countries_data_reader->readAll();
        
        return $countries;
    }
    
    public static function getCountryName($countryCode)
    {
    	$countries_data_reader = Yii::app()->db->createCommand()
    	->select('name')
    	->from('country')
    	->where('code=:code', array(':code'=>$countryCode))
    	->queryRow();
    	$countryName = $countries_data_reader['name'];
    	 
    	return $countryName;    	
    }
    
    public static function getCities($countryCode)
    {
    	$cities_data_reader = Yii::app()->db->createCommand()
    	->select('city, accent_city')
    	->from('city')
    	->where('country=:country', array(':country'=>$countryCode))
    	->order('accent_city')
    	->query();
    	
    	$cities = $cities_data_reader->readAll();
    	
    	return $cities;    
    }

}