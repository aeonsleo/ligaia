<?php

/**
 * This is the model class for table "lig_product_category".
 *
 * The followings are the available columns in table 'lig_product_category':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $area_metrics
 * @property string $area_metrics_plural
 * @property string $area_metrics_abbr
 * @property string $density
 * @property string $units_per_area
 */
class ProductCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_product_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, parent_id, area_metrics, area_metrics_plural, area_metrics_abbr, density, units_per_area', 'required'),
			array('parent_id', 'numerical', 'integerOnly'=>true),
			array('name, area_metrics, area_metrics_plural, area_metrics_abbr, density, units_per_area', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent_id, area_metrics, area_metrics_plural, area_metrics_abbr, density, units_per_area', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Parent',
			'area_metrics' => 'Area Metrics',
			'area_metrics_plural' => 'Area Metrics Plural',
			'area_metrics_abbr' => 'Area Metrics Abbr',
			'density' => 'Density',
			'units_per_area' => 'Units Per Area',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('area_metrics',$this->area_metrics,true);
		$criteria->compare('area_metrics_plural',$this->area_metrics_plural,true);
		$criteria->compare('area_metrics_abbr',$this->area_metrics_abbr,true);
		$criteria->compare('density',$this->density,true);
		$criteria->compare('units_per_area',$this->units_per_area,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
