<?php

/**
 * This is the model class for table "lig_vendor".
 *
 * The followings are the available columns in table 'lig_vendor':
 * @property integer $user_id
 * @property integer $partner_id
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string $organisation_number
 * @property string $established
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $address_line_3
 * @property string $zip
 * @property string $city
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $country_code
 * @property string $province
 *
 * The followings are the available model relations:
 * @property ProductLot[] $productLots
 * @property User $user
 * @property Country $countryCode
 * @property Partner $partner
 */
class Vendor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_vendor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, partner_id, name, firstname, lastname organisation_number, established, city, phone, country_code, province', 'required'),
			array('user_id, partner_id', 'numerical', 'integerOnly'=>true),
			array('name, firstname, lastname, address_line_1, address_line_2, address_line_3', 'length', 'max'=>100),
			array('organisation_number, zip, city, phone, mobile, fax, country_code, province', 'length', 'max'=>50),
			array('established', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, partner_id, name, firstname, lastname, organisation_number, established, address_line_1, address_line_2, address_line_3, zip, city, phone, mobile, fax, country_code, province', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productLots' => array(self::HAS_MANY, 'ProductLot', 'vendor_user_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'countryCode' => array(self::BELONGS_TO, 'Country', 'country_code'),
			'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
			'lotCount' => array(self::STAT, 'ProductLot', 'vendor_user_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'partner_id' => 'Partner',
			'name' => 'Name',
			'organisation_number' => 'Organisation Number',
			'established' => 'Established',
			'address_line_1' => 'Address Line 1',
			'address_line_2' => 'Address Line 2',
			'address_line_3' => 'Address Line 3',
			'zip' => 'Zip',
			'city' => 'City',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'fax' => 'Fax',
			'country_code' => 'Country Code',
			'province' => 'Province',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('organisation_number',$this->organisation_number,true);
		$criteria->compare('established',$this->established,true);
		$criteria->compare('address_line_1',$this->address_line_1,true);
		$criteria->compare('address_line_2',$this->address_line_2,true);
		$criteria->compare('address_line_3',$this->address_line_3,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('province',$this->province,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vendor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
