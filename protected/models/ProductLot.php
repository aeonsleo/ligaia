<?php

/**
 * This is the model class for table "lig_product_lot".
 *
 * The followings are the available columns in table 'lig_product_lot':
 * @property integer $id
 * @property string $serial_no
 * @property string $name
 * @property integer $units
 * @property string $latitude
 * @property string $longitude
 * @property integer $vendor_user_id
 * @property integer $product_id
 *
 * The followings are the available model relations:
 * @property LotUser[] $lotUsers
 * @property Product $product
 * @property Vendor $vendorUser	
 */
class ProductLot extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lig_product_lot';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serial_no, latitude, longitude, vendor_user_id, product_id', 'required'),
			array('units, vendor_user_id, product_id', 'numerical', 'integerOnly'=>true),
			array('serial_no, name, latitude, longitude', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, serial_no, name, units, latitude, longitude, vendor_user_id, product_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lotUsers' => array(self::HAS_MANY, 'LotUser', 'lot_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'vendorUser' => array(self::BELONGS_TO, 'Vendor', 'vendor_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serial_no' => 'Serial No',
			'name' => 'Name',
			'units' => 'Units',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'vendor_user_id' => 'Vendor User',
			'product_id' => 'Product',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serial_no',$this->serial_no,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('units',$this->units);
		$criteria->compare('latitude',$this->latitude,true);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('vendor_user_id',$this->vendor_user_id);
		$criteria->compare('product_id',$this->product_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductLot the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
