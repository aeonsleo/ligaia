		<div id="navbar" class="navbar navbar-inverse hidden-print main navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a id="logo" href="<?php echo Yii::app()->baseUrl?>" class="animated fadeInDown pull-left"><img src="<?php echo Yii::app()->baseUrl?>/images/framework/logo/logo_1.png" alt="" class="img-clean"></a>
				</div>
				<div class="navbar-collapse collapse">
				<?php if(Yii::app()->user->getId()):?>
					<?php $this->widget('zii.widgets.CMenu', array(
						'items'=>array(
							array(
								'label' => '<i class="fa fa-fw icon-graph-up-1"></i> Dashboard</a>', 
								'url'=>array('dashboard/index'),
							),
							array(
								'label'=>'<i class="fa fa-fw icon-cogs"></i> Portfolio</a>', 
								'url'=>array('portfolio/index'),
							),
							array(
								'label'=>'<i class="fa fa-fw icon-collage"></i> Marketplace</a>', 
								'url'=>array('marketplace/index'),
							),
							array(
								'label'=>'<i class="fa fa-fw icon-wrench"></i> Calculate</a>', 
								'url'=>array('calculate/index'),
								'visible'=>'true'
							),
						),
						'encodeLabel' => false,
						'htmlOptions' => array(
							'class'=>'nav navbar-nav main-menu lig-top-menu',
						),
						'submenuHtmlOptions' => array(
							'class' => '',
						)
					)); ?>	
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown dropdown-light">
						<a href="" data-toggle="dropdown" class="dropdown-toggle"><?php print($user->username)?><span class="caret"></span><i class="fa fa-fw icon-user-1 user-icon"></i></a>
						<ul class="dropdown-menu pull-left">
							<li class=""><a href="<?php echo Yii::app()->baseUrl?>/user/profile" class="glyphicons show_big_thumbnails"><i></i><span>Profile</span></a></li>
							<li><a href="<?php echo Yii::app()->baseUrl?>/" class="glyphicons shopping_cart"><i></i><span>Settings</span></a></li>
							<li><a href="<?php echo Yii::app()->baseUrl?>/user/logout" class="glyphicons home"><i></i><span>Logout</span></a></li>
						</ul>
						</li>
					</ul>
					<?php endif;?>
					<div class="clearfix">
					</div>
				</div>
			</div>
		</div>
		<!-- // END navbar -->