-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 15, 2014 at 03:04 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ligaia`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ha_logins`
--

CREATE TABLE IF NOT EXISTS `ha_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `loginProvider` varchar(50) NOT NULL,
  `loginProviderIdentifier` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginProvider_2` (`loginProvider`,`loginProviderIdentifier`),
  KEY `loginProvider` (`loginProvider`),
  KEY `loginProviderIdentifier` (`loginProviderIdentifier`),
  KEY `userId` (`userId`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_auction`
--

CREATE TABLE IF NOT EXISTS `lig_auction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_foreign_key_refs` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `lig_auth_assignment` (
  `bizrule` text,
  `data` text,
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_lig_auth_assignment_lig_user1_idx` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lig_auth_item`
--

CREATE TABLE IF NOT EXISTS `lig_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lig_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `lig_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  KEY `fk_lig_auth_item_child_lig_auth_item1_idx` (`parent`),
  KEY `fk_lig_auth_item_child_lig_auth_item2_idx` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lig_bid`
--

CREATE TABLE IF NOT EXISTS `lig_bid` (
  `id` int(11) NOT NULL,
  `price` int(10) DEFAULT NULL,
  `auction_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_bid_lig_auction1_idx` (`auction_id`),
  KEY `fk_lig_bid_lig_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_email_template`
--

CREATE TABLE IF NOT EXISTS `lig_email_template` (
  `name` varchar(50) NOT NULL,
  `subject` text NOT NULL,
  `from_email` varchar(100) DEFAULT NULL,
  `from_name` varchar(150) DEFAULT NULL,
  `template` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lig_group`
--

CREATE TABLE IF NOT EXISTS `lig_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_group_member`
--

CREATE TABLE IF NOT EXISTS `lig_group_member` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `fk_lig_group_member_lig_group1_idx` (`group_id`),
  KEY `fk_lig_group_member_lig_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_investor`
--

CREATE TABLE IF NOT EXISTS `lig_investor` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `company` varchar(50) NOT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `activation_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_lig_investor_country1_idx` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_invoice`
--

CREATE TABLE IF NOT EXISTS `lig_invoice` (
  `id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `investor_user_id` int(11) NOT NULL,
  `offering_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_invoice_lig_investor1_idx` (`investor_user_id`),
  KEY `fk_lig_invoice_lig_offering1_idx` (`offering_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_lot_user`
--

CREATE TABLE IF NOT EXISTS `lig_lot_user` (
  `lot_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  KEY `fk_lig_lot_user_lig_product_lot1_idx` (`lot_id`),
  KEY `fk_lig_lot_user_lig_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_message`
--

CREATE TABLE IF NOT EXISTS `lig_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `message_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_message_lig_message_type1_idx` (`message_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_message_type`
--

CREATE TABLE IF NOT EXISTS `lig_message_type` (
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_notification`
--

CREATE TABLE IF NOT EXISTS `lig_notification` (
  `id` int(11) NOT NULL,
  `message` char(20) DEFAULT NULL,
  `notification_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_notification_lig_notification_type1_idx` (`notification_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_notification_type`
--

CREATE TABLE IF NOT EXISTS `lig_notification_type` (
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_offering`
--

CREATE TABLE IF NOT EXISTS `lig_offering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `offering_type` varchar(50) NOT NULL,
  `units_per_cycle` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_purchased` int(11) DEFAULT NULL,
  `status_id` int(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_lig_offering_lig_product1_idx` (`product_id`),
  KEY `fk_lig_offering_lig_offering_type1_idx` (`offering_type`),
  KEY `fk_lig_offering_lig_user1_idx` (`user_id`),
  KEY `fk_lig_offering_lig_status1_idx` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_offering_type`
--

CREATE TABLE IF NOT EXISTS `lig_offering_type` (
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_parcel`
--

CREATE TABLE IF NOT EXISTS `lig_parcel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_partner`
--

CREATE TABLE IF NOT EXISTS `lig_partner` (
  `user_id` int(11) NOT NULL,
  `partner_name` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `province` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `organization_number` varchar(50) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `bank_account_number` varchar(50) NOT NULL,
  `bank_swift_code` varchar(50) NOT NULL,
  `iban` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_lig_partner_country1_idx` (`country_code`),
  KEY `fk_lig_partner_lig_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_payment`
--

CREATE TABLE IF NOT EXISTS `lig_payment` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_payment_lig_invoice1_idx` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_plantation`
--

CREATE TABLE IF NOT EXISTS `lig_plantation` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `region` varchar(50) NOT NULL,
  `vendor_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product`
--

CREATE TABLE IF NOT EXISTS `lig_product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `lig_product_variation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_product_lig_product_variation1_idx` (`lig_product_variation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product_category`
--

CREATE TABLE IF NOT EXISTS `lig_product_category` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `parent_id` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_product_category_lig_product_category1_idx` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product_lot`
--

CREATE TABLE IF NOT EXISTS `lig_product_lot` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `units` int(11) NOT NULL DEFAULT '1000',
  `plantation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_product_lot_lig_product1_idx` (`product_id`),
  KEY `fk_lig_product_lot_lig_plantation1_idx` (`plantation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product_type`
--

CREATE TABLE IF NOT EXISTS `lig_product_type` (
  `name` varchar(50) NOT NULL,
  `lig_product_variation_type_type` varchar(50) NOT NULL,
  PRIMARY KEY (`name`),
  KEY `fk_lig_product_type_lig_product_variation_type1_idx` (`lig_product_variation_type_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product_variation`
--

CREATE TABLE IF NOT EXISTS `lig_product_variation` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lat` varchar(50) DEFAULT NULL,
  `long` varchar(50) DEFAULT NULL,
  `product_category_id` int(2) NOT NULL,
  `product_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_product_variation_lig_product_category1_idx` (`product_category_id`),
  KEY `fk_lig_product_variation_lig_product_type1_idx` (`product_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_product_variation_type`
--

CREATE TABLE IF NOT EXISTS `lig_product_variation_type` (
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_shopping_cart`
--

CREATE TABLE IF NOT EXISTS `lig_shopping_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_shopping_cart_lig_user1_idx` (`user_id`),
  KEY `fk_lig_shopping_cart_lig_status1_idx` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_shopping_cart_items`
--

CREATE TABLE IF NOT EXISTS `lig_shopping_cart_items` (
  `id` int(11) NOT NULL,
  `shopping_cart_id` int(11) NOT NULL,
  `offering_id` int(11) NOT NULL,
  `units` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_shopping_cart_items_lig_shopping_cart1_idx` (`shopping_cart_id`),
  KEY `fk_lig_shopping_cart_items_lig_offering1_idx` (`offering_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_status`
--

CREATE TABLE IF NOT EXISTS `lig_status` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  `status_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_status_status_type` (`status`,`status_type`),
  KEY `fk_lig_status_lig_status_type1_idx` (`status_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_status_type`
--

CREATE TABLE IF NOT EXISTS `lig_status_type` (
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_user`
--

CREATE TABLE IF NOT EXISTS `lig_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `status_id` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lig_user_lig_status1_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `lig_user_role`
--

CREATE TABLE IF NOT EXISTS `lig_user_role` (
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lig_vendor`
--

CREATE TABLE IF NOT EXISTS `lig_vendor` (
  `user_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `organisation_number` varchar(50) NOT NULL,
  `established` date NOT NULL,
  `province` varchar(50) NOT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `country_code` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_lig_vendor_lig_partner1_idx` (`partner_id`),
  KEY `fk_lig_vendor_country1_idx` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lig_auction`
--
ALTER TABLE `lig_auction`
  ADD CONSTRAINT `fk_lig_auction_lig_user1` FOREIGN KEY (`created_by`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_auth_assignment`
--
ALTER TABLE `lig_auth_assignment`
  ADD CONSTRAINT `fk_lig_auth_assignment_lig_auth_item1` FOREIGN KEY (`itemname`) REFERENCES `lig_auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_auth_assignment_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `lig_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lig_auth_item_child`
--
ALTER TABLE `lig_auth_item_child`
  ADD CONSTRAINT `fk_lig_auth_item_child_lig_auth_item1` FOREIGN KEY (`parent`) REFERENCES `lig_auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_auth_item_child_lig_auth_item2` FOREIGN KEY (`child`) REFERENCES `lig_auth_item` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_bid`
--
ALTER TABLE `lig_bid`
  ADD CONSTRAINT `fk_lig_bid_lig_auction1` FOREIGN KEY (`auction_id`) REFERENCES `lig_auction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_bid_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_group_member`
--
ALTER TABLE `lig_group_member`
  ADD CONSTRAINT `fk_lig_group_member_lig_group1` FOREIGN KEY (`group_id`) REFERENCES `lig_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_group_member_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_investor`
--
ALTER TABLE `lig_investor`
  ADD CONSTRAINT `fk_lig_investor_country1` FOREIGN KEY (`country`) REFERENCES `country` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_investor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lig_invoice`
--
ALTER TABLE `lig_invoice`
  ADD CONSTRAINT `fk_lig_invoice_lig_investor1` FOREIGN KEY (`investor_user_id`) REFERENCES `lig_investor` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_invoice_lig_offering1` FOREIGN KEY (`offering_id`) REFERENCES `lig_offering` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_lot_user`
--
ALTER TABLE `lig_lot_user`
  ADD CONSTRAINT `fk_lig_lot_user_lig_product_lot1` FOREIGN KEY (`lot_id`) REFERENCES `lig_product_lot` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_lot_user_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_message`
--
ALTER TABLE `lig_message`
  ADD CONSTRAINT `fk_lig_message_lig_message_type1` FOREIGN KEY (`message_type`) REFERENCES `lig_message_type` (`type`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_notification`
--
ALTER TABLE `lig_notification`
  ADD CONSTRAINT `fk_lig_notification_lig_notification_type1` FOREIGN KEY (`notification_type`) REFERENCES `lig_notification_type` (`type`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_offering`
--
ALTER TABLE `lig_offering`
  ADD CONSTRAINT `fk_lig_offering_lig_offering_type1` FOREIGN KEY (`offering_type`) REFERENCES `lig_offering_type` (`type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_offering_lig_product1` FOREIGN KEY (`product_id`) REFERENCES `lig_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_offering_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_offering_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `lig_status` (`id`);

--
-- Constraints for table `lig_partner`
--
ALTER TABLE `lig_partner`
  ADD CONSTRAINT `fk_lig_partner_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_partner_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `country` (`code`);

--
-- Constraints for table `lig_payment`
--
ALTER TABLE `lig_payment`
  ADD CONSTRAINT `fk_lig_payment_lig_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `lig_invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_product`
--
ALTER TABLE `lig_product`
  ADD CONSTRAINT `fk_lig_product_lig_product_variation1` FOREIGN KEY (`lig_product_variation_id`) REFERENCES `lig_product_variation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_product_category`
--
ALTER TABLE `lig_product_category`
  ADD CONSTRAINT `fk_lig_product_category_lig_product_category1` FOREIGN KEY (`parent_id`) REFERENCES `lig_product_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_product_lot`
--
ALTER TABLE `lig_product_lot`
  ADD CONSTRAINT `fk_lig_product_lot_lig_plantation1` FOREIGN KEY (`plantation_id`) REFERENCES `lig_plantation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_product_lot_lig_product1` FOREIGN KEY (`product_id`) REFERENCES `lig_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_product_type`
--
ALTER TABLE `lig_product_type`
  ADD CONSTRAINT `fk_lig_product_type_lig_product_variation_type1` FOREIGN KEY (`lig_product_variation_type_type`) REFERENCES `lig_product_variation_type` (`type`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_product_variation`
--
ALTER TABLE `lig_product_variation`
  ADD CONSTRAINT `fk_lig_product_variation_lig_product_category1` FOREIGN KEY (`product_category_id`) REFERENCES `lig_product_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_product_variation_lig_product_type1` FOREIGN KEY (`product_type`) REFERENCES `lig_product_type` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_shopping_cart`
--
ALTER TABLE `lig_shopping_cart`
  ADD CONSTRAINT `fk_lig_shopping_cart_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_shopping_cart_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `lig_status` (`id`);

--
-- Constraints for table `lig_shopping_cart_items`
--
ALTER TABLE `lig_shopping_cart_items`
  ADD CONSTRAINT `fk_lig_shopping_cart_items_lig_offering1` FOREIGN KEY (`offering_id`) REFERENCES `lig_offering` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lig_shopping_cart_items_lig_shopping_cart1` FOREIGN KEY (`shopping_cart_id`) REFERENCES `lig_shopping_cart` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_status`
--
ALTER TABLE `lig_status`
  ADD CONSTRAINT `fk_lig_status_lig_status_type1` FOREIGN KEY (`status_type`) REFERENCES `lig_status_type` (`type`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lig_user`
--
ALTER TABLE `lig_user`
  ADD CONSTRAINT `lig_user_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `lig_status` (`id`);

--
-- Constraints for table `lig_vendor`
--
ALTER TABLE `lig_vendor`
  ADD CONSTRAINT `fk_lig_vendor_lig_user1` FOREIGN KEY (`user_id`) REFERENCES `lig_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `lig_vendor_ibfk_1` FOREIGN KEY (`country_code`) REFERENCES `country` (`code`),
  ADD CONSTRAINT `lig_vendor_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `lig_partner` (`user_id`);
