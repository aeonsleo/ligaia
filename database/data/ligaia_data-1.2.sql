-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 01, 2014 at 12:30 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ligaia`
--

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`code`, `name`) VALUES
('IN', 'India'),
('US', 'United States');

--
-- Dumping data for table `lig_auction`
--


--
-- Dumping data for table `lig_auth_assignment`
--

INSERT INTO `lig_auth_assignment` (`bizrule`, `data`, `itemname`, `userid`) VALUES
(NULL, NULL, 'superadmin', 1);

--
-- Dumping data for table `lig_auth_item`
--

INSERT INTO `lig_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('createUser', 1, 'Create a user', NULL, NULL),
('deleteUser', 1, 'Delete a user', NULL, NULL),
('investor', 2, 'Investor role', NULL, NULL),
('partner', 2, 'Partner role', NULL, NULL),
('readUser', 1, 'Read a user', NULL, NULL),
('superadmin', 2, 'Superadministrator role', NULL, NULL),
('updateUser', 1, 'Update a user', NULL, NULL);

--
-- Dumping data for table `lig_auth_item_child`
--

INSERT INTO `lig_auth_item_child` (`parent`, `child`) VALUES
('superadmin', 'createUser'),
('superadmin', 'readUser'),
('superadmin', 'updateUser'),
('superadmin', 'deleteUser');

--
-- Dumping data for table `lig_bid`
--


--
-- Dumping data for table `lig_email_template`
--

INSERT INTO `lig_email_template` (`name`, `subject`, `from_email`, `from_name`, `template`) VALUES
('user_verification', 'Do no reply: Email Verification', 'noreply@ligaia.com', 'No Reply', 'Please click the following verification link to activate your account:\r\n\r\n[%verification_link%]\r\n\r\n');

--
-- Dumping data for table `lig_group`
--


--
-- Dumping data for table `lig_group_member`
--


--
-- Dumping data for table `lig_investor`
--

INSERT INTO `lig_investor` (`user_id`, `firstname`, `lastname`, `company`, `address_line_1`, `address_line_2`, `address_line_3`, `zip`, `city`, `country`, `activation_key`) VALUES
(15, 'InvestorAB', 'Srivastava', 'Freelancer', 'X2', 'Harbans Vihar', 'Chimbel', '403006', 'Panaji', 'IN', '605e3417858bd2ed0881a14560176df332a9ade1');

--
-- Dumping data for table `lig_invoice`
--


--
-- Dumping data for table `lig_lot_user`
--


--
-- Dumping data for table `lig_message`
--


--
-- Dumping data for table `lig_message_type`
--


--
-- Dumping data for table `lig_notification`
--


--
-- Dumping data for table `lig_notification_type`
--


--
-- Dumping data for table `lig_offering`
--


--
-- Dumping data for table `lig_partner`
--

INSERT INTO `lig_partner` (`user_id`, `partner_name`, `firstname`, `lastname`, `address_line_1`, `address_line_2`, `address_line_3`, `zip`, `city`, `country_code`, `province`, `phone`, `mobile`, `fax`, `organization_number`, `bank_name`, `bank_account_number`, `bank_swift_code`, `iban`) VALUES
(8, 'Panama 121', 'John', 'Doe', '', '', '', '', 'City101', 'US', 'Panama', '19240998234', '', '', 'A1234aga123', 'ABN Amro', '128u4918734-8', '19274182734-81', '1298341234'),
(9, 'Costa Rica 120', 'John', 'Doe', '', '', '', '', 'City101', 'US', 'Panama', '19240998234', '', '', 'A1234aga123', 'ABN Amro', '128u4918734-8', '19274182734-81', '1298341234'),
(19, 'John Associates', 'John', 'Doe', '', '', NULL, '', 'Lucknow', 'IN', 'UP', '019234096', '', '', 'Joe''s', 'BOI', '19267491267304', '29749068987', '123487812734');

--
-- Dumping data for table `lig_payment`
--


--
-- Dumping data for table `lig_product`
--

INSERT INTO `lig_product` (`id`, `name`, `product_category_id`, `product_type`) VALUES
(1, 'Teak Tree : 1 yr old', 1, 'Teak'),
(2, 'Teak Tree : 2 yr old', 1, 'Teak'),
(4, 'Teak Tree : 3 yr old', 1, 'Teak');

--
-- Dumping data for table `lig_product_category`
--

INSERT INTO `lig_product_category` (`id`, `category`, `parent_id`) VALUES
(1, 'Forestry', 0);

--
-- Dumping data for table `lig_product_lot`
--

INSERT INTO `lig_product_lot` (`id`, `serial_no`, `name`, `units`, `latitude`, `longitude`, `vendor_user_id`, `product_id`) VALUES
(1, 'AAG24141', 'Teak Lot : 3 yr old', 1000, '8.52', '-82.90', 10, 2),
(2, 'AAG24141', 'Test Lot', 1000, '25', '70', 10, 2);

--
-- Dumping data for table `lig_product_type`
--

INSERT INTO `lig_product_type` (`type`) VALUES
('Teak');

--
-- Dumping data for table `lig_product_variation`
--

INSERT INTO `lig_product_variation` (`id`, `name`, `variation_value`, `product_variation_type`, `unit_price`) VALUES
(1, '2 Yr old', 2, 'year', 48),
(2, '3 Yr old', 3, 'year', 50);

--
-- Dumping data for table `lig_product_variation_product`
--

INSERT INTO `lig_product_variation_product` (`product_id`, `product_variation_id`) VALUES
(4, 2);

--
-- Dumping data for table `lig_product_variation_type`
--

INSERT INTO `lig_product_variation_type` (`type`) VALUES
('year');

--
-- Dumping data for table `lig_shopping_cart`
--


--
-- Dumping data for table `lig_shopping_cart_items`
--


--
-- Dumping data for table `lig_status`
--

INSERT INTO `lig_status` (`id`, `status`, `status_type`) VALUES
(3, 'inactive', 'user'),
(1, 'unverified', 'user'),
(2, 'verified', 'user');

--
-- Dumping data for table `lig_status_type`
--

INSERT INTO `lig_status_type` (`type`) VALUES
('cart'),
('offering'),
('user');

--
-- Dumping data for table `lig_user`
--

INSERT INTO `lig_user` (`id`, `username`, `password`, `email`, `last_login`, `status_id`, `created`, `updated`) VALUES
(1, 'administrator', '0192023a7bbd73250516f069df18b500', 'aeonsleo@gmail.com', '2014-06-29 08:44:04', 2, '2014-02-27 12:44:36', '2014-03-01 02:10:08'),
(8, 'panama121', 'aea76dcb8ba40f850c24745a49c784ab', 'panama121@gmail.com', NULL, 1, '2014-03-18 17:05:00', '2014-03-18 17:05:00'),
(9, 'panama122', '14994c4d5d4f1cb69854dad88120d990', 'panama122@gmail.com', NULL, 1, '2014-03-18 17:07:05', '2014-03-18 17:07:05'),
(10, 'ligaia', '0c55b81a9c8f0cb3eaacaa8a73f829c8', 'vendor1@ligaia.com', NULL, 3, '2014-03-18 19:18:11', '2014-03-18 19:18:11'),
(12, 'b5e5a902b9ba18e41e46afeae69b9dc2688196e2', '0c55b81a9c8f0cb3eaacaa8a73f829c8', 'panama1243@gmail.com', NULL, 3, '2014-03-24 17:32:50', '2014-03-24 17:32:50'),
(13, 'ecNOzocUSO', '0c55b81a9c8f0cb3eaacaa8a73f829c8', 'costarica111@gmail.com', NULL, 3, '2014-03-24 17:37:27', '2014-03-24 17:37:27'),
(15, '7_abhishek', 'f6ad6736614b056b55f0f34d38c1bbf2', 'aeonnsleoo@gmail.com', NULL, 1, '2014-05-15 12:19:03', '2014-05-15 12:19:03'),
(19, 'johndoe', 'b9753093a2050e1612428961b756ff38', 'johndoe@yahoo.com', NULL, 1, '2014-06-30 16:32:58', '2014-06-30 16:32:58'),
(27, 'xPuNGvc51q', '0c55b81a9c8f0cb3eaacaa8a73f829c8', 'vendor34@yahoo.com', NULL, 3, '2014-06-30 18:19:24', '2014-06-30 18:19:24');

--
-- Dumping data for table `lig_user_offering`
--


--
-- Dumping data for table `lig_vendor`
--

INSERT INTO `lig_vendor` (`user_id`, `partner_id`, `name`, `firstname`, `lastname`, `organisation_number`, `established`, `address_line_1`, `address_line_2`, `address_line_3`, `zip`, `city`, `phone`, `mobile`, `fax`, `country_code`, `province`) VALUES
(10, 8, 'Vendor Panama 121', '', '', 'PW123415', '0000-00-00', '', '', '', '', 'San Fransisco', '091240618273057', '', '', 'US', 'California'),
(12, 8, 'Teak Vendor Panama', '', '', '123413414', '0000-00-00', '', '', '', '', 'Panama', '124253125125', '', '', 'US', 'San jose'),
(13, 9, 'Wood Costa', '', '', '082740824', '0000-00-00', '', '', '', '', 'Costa rica', '125153145', '', '', 'US', 'San jose'),
(27, 19, 'John''s Vendor', 'Vendor Joe', 'Stanford', 'Joe''s Cafe', '07/14/2014', '', '', NULL, '', 'City100', '112412414', '', '', 'US', 'Kentucky');

--
-- Dumping data for table `tbl_migration`
--

