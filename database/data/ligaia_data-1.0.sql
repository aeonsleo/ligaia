-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2014 at 07:26 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ligaia`
--

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`code`, `name`) VALUES
('IN', 'India'),
('US', 'United States');

--
-- Dumping data for table `ha_logins`
--


--
-- Dumping data for table `lig_auction`
--


--
-- Dumping data for table `lig_auth_assignment`
--

INSERT INTO `lig_auth_assignment` (`bizrule`, `data`, `itemname`, `userid`) VALUES
(NULL, NULL, 'superadmin', 23);

--
-- Dumping data for table `lig_auth_item`
--

INSERT INTO `lig_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('createUser', 1, 'Create a user', NULL, NULL),
('deleteUser', 1, 'Delete a user', NULL, NULL),
('investor', 2, 'Investor role', NULL, NULL),
('partner', 2, 'Partner role', NULL, NULL),
('readUser', 1, 'Read a user', NULL, NULL),
('superadmin', 2, 'Superadministrator role', NULL, NULL),
('updateUser', 1, 'Update a user', NULL, NULL);

--
-- Dumping data for table `lig_auth_item_child`
--

INSERT INTO `lig_auth_item_child` (`parent`, `child`) VALUES
('superadmin', 'createUser'),
('superadmin', 'readUser'),
('superadmin', 'updateUser'),
('superadmin', 'deleteUser');

--
-- Dumping data for table `lig_bid`
--


--
-- Dumping data for table `lig_email_template`
--

INSERT INTO `lig_email_template` (`name`, `subject`, `from_email`, `from_name`, `template`) VALUES
('user_verification', 'Do no reply: Email Verification', 'noreply@ligaia.com', 'No Reply', 'Please click the following verification link to activate your account:\r\n\r\n[%verification_link%]\r\n\r\n');

--
-- Dumping data for table `lig_group`
--


--
-- Dumping data for table `lig_group_member`
--


--
-- Dumping data for table `lig_investor`
--

INSERT INTO `lig_investor` (`user_id`, `firstname`, `lastname`, `company`, `address_line_1`, `address_line_2`, `address_line_3`, `zip`, `city`, `country`, `activation_key`) VALUES
(23, 'Abhishek', 'Srivastava', 'Innowiti', 'X2 Harbans Vihar', 'AWHO Colony', 'Chimbel, Tiswadi', '403006', 'Panaji', 'IN', '2655398251ed8c6f2b2bc4326caea010bba7ce93'),
(24, 'Abhishek', 'Srivastava', 'innowiti', '', '', '', '', 'Panjim', 'IN', '97903b7abb22f9e68fe95e6df8a46e917ee81537');

--
-- Dumping data for table `lig_invoice`
--


--
-- Dumping data for table `lig_lot_user`
--


--
-- Dumping data for table `lig_message`
--


--
-- Dumping data for table `lig_message_type`
--


--
-- Dumping data for table `lig_notification`
--


--
-- Dumping data for table `lig_notification_type`
--


--
-- Dumping data for table `lig_offering`
--


--
-- Dumping data for table `lig_offering_type`
--


--
-- Dumping data for table `lig_parcel`
--


--
-- Dumping data for table `lig_partner`
--


--
-- Dumping data for table `lig_payment`
--


--
-- Dumping data for table `lig_plantation`
--


--
-- Dumping data for table `lig_product`
--


--
-- Dumping data for table `lig_product_category`
--


--
-- Dumping data for table `lig_product_lot`
--


--
-- Dumping data for table `lig_product_type`
--


--
-- Dumping data for table `lig_product_variation`
--


--
-- Dumping data for table `lig_product_variation_type`
--


--
-- Dumping data for table `lig_shopping_cart`
--


--
-- Dumping data for table `lig_shopping_cart_items`
--


--
-- Dumping data for table `lig_status`
--

INSERT INTO `lig_status` (`id`, `status`, `status_type`) VALUES
(1, 'unverified', 'user'),
(2, 'verified', 'user');

--
-- Dumping data for table `lig_status_type`
--

INSERT INTO `lig_status_type` (`type`) VALUES
('cart'),
('offering'),
('user');

--
-- Dumping data for table `lig_user`
--

INSERT INTO `lig_user` (`id`, `username`, `password`, `email`, `last_login`, `created`, `updated`, `status_id`) VALUES
(23, 'aeonsleo', 'f7a85ef6dd1c6c5e3f57788f8773cf4e', 'aeonsleo@gmail.com', '2014-03-03 22:02:05', '2014-02-27 12:44:36', '2014-03-01 02:10:08', 2),
(24, 'abhishek', 'f7a85ef6dd1c6c5e3f57788f8773cf4e', 'abhishek@gmail.com', '2014-03-03 22:00:29', '2014-03-02 22:42:53', '2014-03-02 22:42:53', 1);

--
-- Dumping data for table `lig_user_role`
--


--
-- Dumping data for table `lig_vendor`
--


--
-- Dumping data for table `tbl_migration`
--

