$(function(){
	
    /*
     * Ligaia Custom Slider: Volume slider
     */
	if ($('.volume-slider').size() > 0)
	{
		$( "#volumeSlider" ).slider({
			create: JQSliderCreate,
            range: "min",
            value: 20,
            min: 1,
            max: 50,
            slide: function( event, ui ) {
                $( ".volume-slider .amount" ).val( ui.value );
                $( "span#volume" ).html( ui.value );
            },
            start: function() { if (typeof mainYScroller != 'undefined') mainYScroller.disable(); },
	        stop: function() { if (typeof mainYScroller != 'undefined') mainYScroller.enable(); }
        });
        $( ".volume-slider .amount" ).val( $( ".volume-slider .slider" ).slider( "value" ) );
        $( "span#volume" ).html(  $( ".volume-slider .slider" ).slider( "value" ) );
	}
     
    /*
     * Ligaia Custom Slider: Age slider
     */
	if ($('.age-slider').size() > 0)
	{
		$( ".age-slider .slider" ).slider({
			create: JQSliderCreate,
            range: "min",
            value: 1,
            min: 1,
            max: 25,
            slide: function( event, ui ) {
                $( ".age-slider .amount" ).val( ui.value );
                $( "span#age" ).html( ui.value );
                $("#investmentRangeSlider").rangeSlider("values", ui.value, 12);
            },
            start: function() { if (typeof mainYScroller != 'undefined') mainYScroller.disable(); },
	        stop: function() { if (typeof mainYScroller != 'undefined') mainYScroller.enable(); }
        });
        $( ".age-slider .amount" ).val( $( ".age-slider .slider" ).slider( "value" ) );
        $( "span#age" ).html(  $( ".age-slider .slider" ).slider( "value" ) );
	}
     
    /*
     * Ligaia Custom Slider: Investment slider
     */
	if ($('.investment-slider').size() > 0)
	{
		$( ".investment-slider .slider" ).slider({
			create: JQSliderCreate,
            range: "min",
            value: 500,
            min: 1,
            max: 5000,
            slide: function( event, ui ) {
                $( ".investment-slider .amount" ).val( "$" + ui.value );
                $( "span#investment-amount" ).html( ui.value );

            },
            start: function() { if (typeof mainYScroller != 'undefined') mainYScroller.disable(); },
	        stop: function() { if (typeof mainYScroller != 'undefined') mainYScroller.enable(); }
        });
        $( ".investment-slider .amount" ).val( "$" +  $( ".investment-slider .slider" ).slider( "value" ) );
        $( "span#investment-amount" ).html(  $( ".investment-slider .slider" ).slider( "value" ) );
	}

	$("#investmentRangeSlider").bind("valuesChanging", function(e, data){
		$("#max").html(Math.round(data.values.max));
		var estimateLevel = getEstimateDisplayLevel(data.values.max);
		setEstimateDisplay(estimateLevel);
	});
	
	$(".volume-slider .slider").on("slide", function(e, data){
		  var volume = parseInt(data.value);
		  var amount = parseInt($("#investment-amount").html());
		  
		  invLevel = getInvestmentDisplayLevel(amount, volume);
		  setInvDisplayLevel(invLevel);
	});

	$(".investment-slider .slider").on("slide", function(e, data){
		  var amount = parseInt(data.value);
		  var volume = parseInt($("#volume").html());
		  
		  invLevel = getInvestmentDisplayLevel(amount, volume);
		  setInvDisplayLevel(invLevel);
	});

});

function getInvestmentDisplayLevel(amount, volume)
{
	var invLevel;
	console.log(amount);
	console.log(volume);
	
	if(volume <= 10)
	{
		if(amount <= 360)
		{
			invLevel = 1;
		}
		else
		{
			invLevel = 2;
		}
	}
	else if(volume <= 50)
	{
		if(amount <= 1300)
		{
			invLevel = 3;
		}
		else if(amount < 3000)
		{
			invLevel = 4;
		}
		else 
		{
			invLevel = 5;
		}
	}
	else
	{
		invLevel = 5;
	}
	
	return invLevel;
}

function setInvDisplayLevel(level)
{
	$("#inv-level-gr li").removeClass("active");
	$("#inv-level-gr li").each(function(){
		var index = $(this).index();
		if(index < level)
		{
			$(this).addClass("active");
		}
		
	});
}

function getEstimateDisplayLevel(age)
{
	var estLevel;
	
	if(age < 4)
	{
		estLevel = 1;
	}
	else if(age < 9)
	{
		estLevel = 2;
	}
	else if(age < 14)
	{
		estLevel = 3;
	}
	else if(age < 17)
	{
		estLevel = 4;
	}
	else
	{
		estLevel  = 5;
	}
	
	return estLevel;
}

function setEstimateDisplay(level)
{
	$("#est-level-gr li").removeClass("active");
	$("#est-level-gr li").each(function(){
		var index = $(this).index();
		if(index < level)
		{
			$(this).addClass("active");
		}
		
	});
}